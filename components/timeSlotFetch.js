import React, { Component } from 'react';
import moment from 'moment';
import firebase from '../firebase';


export const timeSlotsFetch = (courtEmail, date, callbackSlots) => {
  const email = courtEmail;
  //console.log("inside timeslotfetch " + email);
  //const date = date;
  //console.log("inside date" + firebase.firestore().collection("court").doc(email).collection('availabletimeslots').doc(date).get().holiday);
  firebase.firestore().collection("court").doc(email).onSnapshot((documentSnapshot) => {

    const timeslots = documentSnapshot.get('timeSlot');


    firebase.firestore().collection('court').doc(email).collection('availabletimeslots').doc(date).onSnapshot((documentSnapshot) => {
      const holiday = documentSnapshot.get('holiday');
      console.log("holiday status " + holiday);

      if (holiday) {
        //return 0;
        var availabile = 0;
        callbackSlots(0);
      }
      else {
        firebase.firestore().collection('court').doc(email).collection('availabletimeslots').doc(date).collection('timeslots').onSnapshot((querySnapshot) => {
          var availabile = timeslots;
          querySnapshot.forEach(slot => {
            const booked = slot.get('booked');
            //console.log("bookings "+booked);
            //console.log("bookings "+ slot.id);
            if (booked) {
              availabile.splice(availabile.indexOf(slot.id), 1);
            }

          });
          console.log("available list " + availabile);
          //this.setState({availableSlots: availabile});
          //return availabile;
          callbackSlots(availabile);
        });
      }

    });

    console.log(timeslots);
    console.log(date);
    //console.log(holiday);
  });


}

  //export default timeSlotsFetch;