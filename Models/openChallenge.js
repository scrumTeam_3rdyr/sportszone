class OpenChallenge{
    constructor(requestId,opponentTeamId,opponentTeamName,opponentTeamLogoUrl,date,time,courtName,courtEmail,type,matchStatus){
        this.requestId = requestId,
        this.opponentTeamId = opponentTeamId,
        this.opponentTeamName = opponentTeamName,
        this.opponentTeamLogoUrl = opponentTeamLogoUrl,
        this.date = date,
        this.time = time,
        this.courtName = courtName,
        this.courtEmail = courtEmail,
        this.type = type,
        this.matchStatus = matchStatus
    }
}
export default OpenChallenge;