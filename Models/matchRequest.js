class MatchRequest{
    constructor(requestId,teamId,teamName,teamLogoUrl,opponentTeamId,opponentTeamName,opponentTeamLogoUrl,date,time,courtName,courtEmail,type,matchStatus){
        this.requestId = requestId,
        this.teamId=teamId,
        this.teamName=teamName,
        this.teamLogoUrl=teamLogoUrl,
        this.opponentTeamId = opponentTeamId,
        this.opponentTeamName = opponentTeamName,
        this.opponentTeamLogoUrl = opponentTeamLogoUrl,
        this.date = date,
        this.time = time,
        this.courtName = courtName,
        this.courtEmail = courtEmail,
        this.type = type,
        this.matchStatus = matchStatus
    }
}
export default MatchRequest;