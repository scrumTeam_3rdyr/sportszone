import React, { useState, useReducer, useCallback, useEffect } from 'react';
import { StyleSheet, ScrollView, Dimensions, Alert, KeyboardAvoidingView, Picker, TouchableOpacity, TextInput } from 'react-native';
import { Block, Text, theme, Button } from 'galio-framework';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import materialTheme from '../constants/Theme';
import Input from '../components/Input';
import Team from '../Models/team';
import Court from '../Models/court';
import firebase from '../firebase';
import Autocomplete from 'react-native-autocomplete-input';
import moment from 'moment';//import RNPickerSelect from 'react-native-picker-select';
const { height, width } = Dimensions.get('screen');

const getTeams = async () => {
    const db = firebase.firestore()

    const ref = db.collection("team");
    const teamsSnapshot = await ref.get()
    // console.log('ref',teamsSnapshot);
    const teams = teamsSnapshot.docs.map(teamSnapshot => {
        return teamSnapshot.data();
    })
    //  console.log("teams",teams);
    const user = firebase.auth().currentUser.email;

    const loadedteams = [];
    for (const key in teams) {
        // console.log('bbbbbbbbbb', teams[key].id)
        if (teams[key].teamCaptain === user) {
            loadedteams.push(new Team(
                teams[key].id,
                teams[key].teamName,
                teams[key].imageUrl,
                teams[key].currentPlayers,
                teams[key].teamCaptain,
                teams[key].loss,
                teams[key].wins
            ));
        };

    }
    return loadedteams;
}

const getAllCourtTimeslots = async (court) => {
    //console.log("hi threreeeeeeeeeeeeeee",court,date);
    const db = firebase.firestore();
    const ref = db.collection("court");
    const courtSnapshot = await ref.get();
    const courts = courtSnapshot.docs.map(courtSnapshot => {
        return courtSnapshot.data();
    })
    //console.log("court",court);
    let allCourtTimeslots = [];
    for (const key in courts) {
        //console.log('bbbbbbbbbb', courts[key].email)
        if (courts[key].email === court) {
            //console.log("all time slots are", courts[key].timeSlot)
            allCourtTimeslots.push(courts[key].timeSlot);
            //return courts[key].timeSlot
        };

    }

    return allCourtTimeslots;

}

const getAllocatedTimeslots = async (court, date) => {
    //const AllTimeSlots = await getAllCourtTimeslots(court);
    // console.log("hi threreeeeeeeeeeeeeee",court,date);
    const db = firebase.firestore();
    const ref = db.collection("court").doc(court).collection("availabletimeslots").doc(date).collection("timeslots");
    const timeslotsSnapshot = await ref.get();
    const timeslots = timeslotsSnapshot.docs.map(timeslotSnapshot => {
        return timeslotSnapshot.data();
    })
    let bookedTimeslots = []
    for (const key in timeslots) {
        // console.log('bbbbbbbbbb', courts[key].email)
        if (timeslots[key].booked === true) {
            bookedTimeslots.push(timeslots[key].timeslot)
            // for (let index = 0; index < AllTimeSlots.length; index++) {
            //     if (AllTimeSlots[index] != timeslots[key].timeslot) availableTimeslots.push(AllTimeSlots[index]);

            // }
            // allocatedTimeslots.push(timeslots[key].timeslot);
            
        }
       
    }
    console.log("Bokked time slots",bookedTimeslots);
    return bookedTimeslots;
    //console.log("available not    ",ref.id,ref);
}

const isHoliday = async (court, date) => {
  // const AllTimeSlots =  await getAllCourtTimeslots(court);
  //  console.log("hi threreeeeeeeeeeeeeee minuriiiiiiiiiii",court,date,AllTimeSlots);
    const db = firebase.firestore();
    const ref = db.collection("court").doc(court).collection("availabletimeslots").doc(date);
    const timeslotsSnapshot = await ref.get();
    if (timeslotsSnapshot.exists && timeslotsSnapshot.data().holiday === true) {// not a holiday
         return true;
    }    
    return false;
}


const getCourts = async () => {
    const db = firebase.firestore()

    const ref = db.collection("court");
    const courtsSnapshot = await ref.get()
    // console.log('ref',teamsSnapshot);
    const courts = courtsSnapshot.docs.map(courtsSnapshot => {
        return courtsSnapshot.data();
    })
    const loadedcourts = [];
    for (const key in courts) {
        // console.log('bbbbbbbbbb', teams[key].id)

        loadedcourts.push(new Court(
            courts[key].address,
            courts[key].court_name,
            courts[key].email,

        ));


    }
    return loadedcourts;
}


const NewMatchStartScreen = props => {

    const [teamData, setTeamData] = useState([]);
    const [courtData, setCourtData] = useState([]);
    const [timeslotData, settimeslotData] = useState([]);
    const [team, setTeam] = useState();
    const [timeslot, setTimeslot] = useState();
    const [court, setCourt] = useState();
    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
    const [Date, setDate] = useState('');
    const [dDate, setdDate] = useState('');
    const [filteredCourts, setFilteredCourts] = useState([]); // Filtered data
    const [selectedValue, setSelectedValue] = useState({}); // selected data
    // const [filteredTimeslots, setFilteredTimeslots] = useState([]);
    // const [selectedTimeslot, setSelectedTimeslot] = useState();

    useEffect(() => {
        const getTeamData = async () => {
            const data = await getTeams();
            setTeamData(data);
        };
        getTeamData();

    }, []);

    useEffect(() => {
        const getCourtData = async () => {
            const data = await getCourts();
            setCourtData(data);

        };
        getCourtData();

    }, []);

    // useEffect(() => {
    //     const getTimeslotData = async () => {
    //         const tdata = await getTimeSlots(selectedValue.email,dDate);
    //         settimeslotData(tdata);


    //     };
    //     getTimeslotData();

    // }, [dDate]);

    // const ex = async()=>{
    //     const tdata = await getTimeSlots(selectedValue.email);
    //         settimeslotData(tdata);
    // }

    const renderTeam = () => {
        const teamList = teamData.map(function (val, index) {
            return <Picker.Item label={val.teamName} value={val.id} />
        });
        return (
            <Picker
                selectedValue={team}
                mode='dropdown'
                itemStyle={{ textAlign: "center" }}
                onValueChange={(itemValue, itemIndex) => {
                    setTeam(itemValue);
                }}>
                {/* <Picker.Item label="Select Team" value="null"/> */}
                {teamList}
            </Picker>

        )
    }

    const renderTimeslots = () =>{
        // const timeslotList = timeslotData.map(function (val, index) {// teamData  is fetched timeslots
        //     return <Picker.Item label={val.teamName} value={val.id} /> // this should be chanhed
        // });
       //const timeslotList = timeslotData;
        const timeslotList = timeslotData.map((val, index)=> {// teamData  is fetched timeslots
            return <Picker.Item label={val} value={val} /> // this should be chanhed
        });
        return (
            <Picker
                selectedValue={timeslot}
                mode='dropdown'
                itemStyle={{ textAlign: "center" }}
                onValueChange={(itemValue, itemIndex) => {
                    setTimeslot(itemValue);
                }}>
                {/* <Picker.Item label="Select Team" value="null"/> */}
                {timeslotList}
                {/* <Picker.Item label="Select Team" value="null"/>
                <Picker.Item label="Select Team" value="null"/> */}

            </Picker>

        )
    }



    const showDatePicker = () => {
        setDatePickerVisibility(true);
    };
    const hideDatePicker = () => {
        setDatePickerVisibility(false);
    };
    const handleConfirmDate = useCallback (async (date) => {
        console.log("Handle confirmation hit");
        setDate(moment(date).format("MMM Do YY"));
        //setdDate(moment(date).format("DDMMMYYYY"));
        setDatePickerVisibility(false);
        //setdDate(moment(date).format("DDMMMYYYY"));
        const tdata = await getAllCourtTimeslots(selectedValue.email);
        
        const bookingdata = await getAllocatedTimeslots(selectedValue.email, moment(date).format("DDMMMYYYY"));
        const holiday = await isHoliday(selectedValue.email, moment(date).format("DDMMMYYYY"));
        //settimeslotData(tdata);
        

        let availableTimeSlots = [];
        if(holiday){
            console.log("It is a holiday");
            Alert.alert('Sorry the court is closed!! ','Select another day', [{ text: 'okay' }]);
            // return;
        }
        else{
         for (let index = 0; index < tdata[0].length; index++) {
             let booked = false;
             for (let i = 0; i < bookingdata.length; i++) {
                if(tdata[0][index] === bookingdata[i]){
                     booked = true;
                }
                 
             }
             if(booked === false){
                 console.log("booked is false");
                 availableTimeSlots.push(tdata[0][index]);
             }
              
          }
          if(availableTimeSlots.length === 0){
            Alert.alert('Sorry the court is closed!! ','Select another day', [{ text: 'okay' }]);
            // return;
          }
        }
        settimeslotData(availableTimeSlots);
        console.log("Allllll", tdata,bookingdata,holiday,availableTimeSlots,tdata[0]);

    },[settimeslotData,setDate,setDatePickerVisibility,selectedValue]) 

    const submitHandler = useCallback(() => {
        console.log(team, Date, dDate, selectedValue.court_name, selectedValue.email, timeslotData,teamData,timeslotData[0],timeslot);
        props.navigation.navigate('NewMatchSelectTeam', {
            teamId: team,
            courtName: selectedValue.court_name,
            courtEmail: selectedValue.email,
            date: Date,
            timeslot: timeslot

        });
    }, [team, Date, selectedValue,timeslot]);



    const findFilm = (query) => {
        if (query) {
            setFilteredCourts(
                courtData.filter((item) => {
                    const itemData = `${item.court_name.toUpperCase()}`;
                    const textData = query.toUpperCase();
                    return itemData.indexOf(textData) > -1;
                })
            );
        } else {

            setFilteredCourts([]);
        }
    };

    return (
        <Block style={styles.screen}>
            <ScrollView>
                <Block style={styles.conatainer}>
                    <Text style={styles.label}>Select your team</Text>
                    <Block style={styles.inputContainerPicker}>
                        {renderTeam()}
                    </Block>
                </Block>
                <Block style={styles.conatainerCourt}>
                    <Text style={styles.label}>Select a court</Text>
                    <Autocomplete
                        data={filteredCourts}
                        defaultValue={selectedValue.court_name}
                        style={{
                            borderWidth: 1,
                            borderRadius: 7,
                            width: 350,
                            height: 40,
                            padding: 10
                        }}
                        containerStyle={styles.autocompleteContainer}
                        placeholderTextColor='gray'
                        onChangeText={text => findFilm(text)}
                        placeholder="Enter the film title"
                        renderItem={({ item }) => (//you can change the view you want to show in suggestions
                            <TouchableOpacity
                                style={{ height: 30 }}
                                onPress={() => {
                                    setSelectedValue(item);
                                    setFilteredCourts([]);
                                }}>
                                <Text >
                                    {item.court_name}
                                </Text>
                            </TouchableOpacity>
                        )}>
                    </Autocomplete>
                </Block>

                <Block style={styles.conatainer}>
                    <Text style={styles.label}>Select Date</Text>
                    <Block style={styles.inputContainer}>
                        <TextInput
                            id='date'
                            label='Date:'
                            placeholder='Date'
                            placeholderTextColor='gray'
                            errorText='Please enter a valid Date'
                            onFocus={showDatePicker}
                            //onInputChange={date =>setDate(date)}
                            value={Date}
                            //initialValue=''
                            inputValue={Date}
                            //initiallyValid={false}
                            required
                            disabled
                        ></TextInput>
                        <DateTimePickerModal
                            isVisible={isDatePickerVisible}
                            mode="date"
                            onConfirm={handleConfirmDate}
                            onCancel={hideDatePicker} />
                    </Block>
                </Block>

                <Block style={styles.conatainer}>
                    <Text style={styles.label}>Select Time</Text>
                    <Block style={styles.inputContainerPicker}>
                        {renderTimeslots()}
                    </Block>
                </Block>
                <Block style={styles.buttonContainer}>
                    <Button
                        shadowless
                        style={styles.button}
                        color={materialTheme.COLORS.SUCCESS}
                        round
                        textStyle={styles.buttonText}
                        onPress={submitHandler}
                        title="Next"
                    >Next</Button>
                </Block>
            </ScrollView>
        </Block>
    );
};
const styles = StyleSheet.create({

    screen: {
        flex: 1,
        backgroundColor: theme.COLORS.WHITE
        // justifyContent: 'center',
        // alignContent: 'center'
    },
    conatainer: {
        //borderWidth:1,
        marginVertical: 20,
        marginHorizontal: 15,
        width: 350,
        height: 70
    },
    conatainerCourt: {
        //borderWidth:1,
        marginVertical: 20,
        marginHorizontal: 15,
        width: 350,
        height: 120,

    },
    autocompleteContainer: {
        backgroundColor: 'red',
        borderWidth: 1,
    },
    label: {
        fontSize: 15,
        fontWeight: 'bold',
        color: 'black'
    },
    inputContainer: {
        borderWidth: 1,
        borderRadius: 10,
        marginTop: 10,
        width: 350,
        height: 40,
        padding: 10

    },
    inputContainerPicker: {
        borderWidth: 1,
        borderRadius: 7,
        marginTop: 10,
        width: 350,
        height: 40,


    },

    autocompleteContainer: {
        flex: 1,
        left: 0,
        position: 'relative',
        right: 0,
        top: 0,
        zIndex: 1,
        backgroundColor: 'transparent'
    },




    // container: {
    //     flex: 1,
    //     backgroundColor: theme.COLORS.WHITE
    // },
    form: {
        flex: 1,
        alignContent: 'center',
        justifyContent: 'center',
        margin: theme.SIZES.BASE * 2,
        marginTop: theme.SIZES.BASE * 5
    },
    button: {
        width: width - theme.SIZES.BASE * 15,
        height: theme.SIZES.BASE * 3,
        shadowRadius: 0,
        shadowOpacity: 0,
    },
    buttonContainer: {
        marginLeft: theme.SIZES.BASE * 12
    },
    buttonText: {
        color: 'black'
    },

});
export default NewMatchStartScreen;