import React,{useState, useEffect} from 'react';
import { StyleSheet, Dimensions, ScrollView, FlatList,View,Image,TouchableOpacity,Picker, RefreshControl } from 'react-native';
import { Button, Block, Text, Input, theme } from 'galio-framework';
import { Icon, Product } from '../components/';
import Theme from '../constants/Theme';

import firebase from '../firebase';
const { width } = Dimensions.get('screen');

const wait = (timeout) => {
  return new Promise(resolve => {
    setTimeout(resolve, timeout);
  });
}


function MyMatches(props){
  
    const teams= firebase.firestore().collection('team');
    const matches=firebase.firestore().collection('matches')
    const [team,setTeam]= useState('empty');
    const [data,setData]=useState([]);
    const [matchData,setMatchData]=useState([]);
    const user = firebase.auth().currentUser.email;
    const [refreshing, setRefreshing] = React.useState(false);


    
//loading team list
  useEffect(()=>{
    let print;
    findteams().then((result)=>{
    print=result.map(function(val,index){
    return {'teamName' : val.data().teamName, 'id':val.id} ;
    })
    setData(print);
  });
},[])

//refreshing matches list after updating team
useEffect(()=>{
  if(team!='null'){
    findmatches().then((result)=>{
      let matchlist=result.map(function(val,index){
        return val.data();
      })
      
      const sortedMatchList = matchlist.sort((a,b)=>{b.date-a.date});
      console.log(sortedMatchList);
      setMatchData(sortedMatchList);
    })
  }
  },[team,refreshing])
  
  const findteams= async ()=>{
    
    const querySnapshot = await teams.where('teamCaptain', '==', user).get();
    return querySnapshot.docs;
  }
  const findmatches=async ()=>{
    console.log(team);

    const awayTeam = matches.where('teamId', '==', team).where('matchStatus','==','accepted').get();
    const homeTeam = matches.where('opponentTeamId', "==",team).where('matchStatus','==','accepted').get();
    const [awaySnapshot,homeSnapshot]=await Promise.all([
      awayTeam,
      homeTeam
    ])
    console.log(awaySnapshot.docs);
    const awayTeamArray = awaySnapshot.docs;
    const homeTeamAray = homeSnapshot.docs;
    const querySnapshot = awayTeamArray.concat(homeTeamAray);
    //console.log(querySnapshot.docs);
    return querySnapshot;
  }
   const renderTeam = () =>{
    // const [teamList, setTeamList] = useState([]);
  //console.log( "user is",user)
    const teamList=data.map(function(val,index){
      //console.log(val.teamName)
      return <Picker.Item label={val.teamName} value={val.id}/>
    });
    return(
      <Picker
        selectedValue={team}
        itemStyle={{textAlign:"center"}}
        onValueChange={(itemValue, itemIndex) =>{
            console.log(itemIndex + " "+ itemValue);
            setMatchData([]);
            setTeam(itemValue);
          }
        }>
        <Picker.Item label="Select Team" value="null"/>
        {teamList}
    </Picker>    
      
    )
   }

  const opponent=(item)=>{
    if(item.teamId==team){
      return(
        <>
        <Image source = {{uri:item.opponentTeamLogoUrl}} style={styles.imageView} />
        <View style={{flex:2, flexDirection: 'column',justifyContent:"center"}}>
            <Text size={16}>{item.opponentTeamName}</Text>
        </View>
        </>
      )
    }
    else if(item.opponentTeamId==team){
      return(
      <>
        <Image source = {{uri:item.teamLogoUrl}} style={styles.imageView} />
        <View style={{flex:2, flexDirection: 'column',justifyContent:"center"}}>
            <Text size={16}>{item.teamName}</Text>
        </View>
        </>
        )
    }
  }
  const renderMatches = () => {
    return (
     
      <FlatList
           data={matchData}
           renderItem={({item})=>
           <View style={styles.list}>
        
          {opponent(item)}
           
           <View style={{flex:4, flexDirection: 'column', alignItems:"flex-end",justifyContent:"center", paddingRight:20}}>
           <Text>{item.time}</Text>
           <Text >{item.date}</Text>
           <Text>{item.venue}</Text>
           </View>
         </View>
         }
         keyExtractor={item => item.requestId}
         />


 )
}
    const onRefresh = React.useCallback(() => {
      setRefreshing(true);

      wait(1000).then(() => setRefreshing(false));
    }, []);


    const {navigation} = props;
    return (
      <Block flex style={styles.home}>
        {renderTeam()}
        <ScrollView refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />}>
        {renderMatches()}
        </ScrollView>
        
        <View>
        <TouchableOpacity
          //activeOpacity={0.7}
          onPress={()=>navigation.navigate('New Match')}
          style={styles.TouchableOpacityStyle}>
          <Icon name="plus-circle" family="material-community" size={55}
          color="#3BAD36" backgroundColor="#fff"
          />
        </TouchableOpacity>
        </View>
      </Block>
    );
    }

    export default MyMatches;

const styles = StyleSheet.create({
  home: {
    width: width,    
  },
  search: {
    height: 48,
    width: width - 32,
    marginHorizontal: 16,
    borderWidth: 1,
    borderRadius: 3,
  },
  header: {
    backgroundColor: theme.COLORS.WHITE,
    shadowColor: theme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 8,
    shadowOpacity: 0.2,
    elevation: 4,
    zIndex: 2,
  },
  tabs: {
    marginBottom: 24,
    marginTop: 10,
    elevation: 4,
  },
  tab: {
    backgroundColor: theme.COLORS.TRANSPARENT,
    width: width * 0.50,
    borderRadius: 0,
    borderWidth: 0,
    height: 24,
    elevation: 0,
  },
  tabTitle: {
    lineHeight: 19,
    fontWeight: '300'
  },
  divider: {
    borderRightWidth: 0.3,
    borderRightColor: theme.COLORS.MUTED,
  },
  products: {
  //  width: width - theme.SIZES.BASE * 2,
  //  paddingVertical: theme.SIZES.BASE * 2,
    flex:1,
    flexDirection:"row",
  },
  list:{
    flex:1, 
    flexDirection: 'row', 
    justifyContent:"space-evenly",
    backgroundColor:"#fff",
    //borderRadius:25,
    borderBottomWidth:10,
    borderBottomColor:Theme.COLORS.BORDER_COLOR,
    paddingVertical:10,
  },
  imageView: {
    width: width/5,
    height: width/5 ,
    margin: 7,
    borderRadius : 7,
},
TouchableOpacityStyle: {
  //Here is the trick
  position: 'absolute',
  width: 50,
  height: 50,
  alignItems: 'center',
  justifyContent: 'center',
  right: 35,
  bottom: 35,
  backgroundColor:"white",
  borderRadius:40
},
FloatingButtonStyle: {
  resizeMode: 'contain',
  width: 50,
  height: 50,
  //backgroundColor:'black'
},

});
