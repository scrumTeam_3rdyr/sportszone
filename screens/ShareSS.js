import React,{ useState, useEffect } from 'react';
import { StyleSheet, Dimensions, ScrollView, Image, ImageBackground, Platform,View,ToastAndroid, Share,Text, PixelRatio } from 'react-native';
import { Block, theme,Input,Toast,Button} from 'galio-framework';
import { HeaderHeight } from "../constants/utils";
import { Avatar ,Icon} from 'react-native-elements';
//firebase
import firebase from '../firebase';
import 'firebase/firestore';
import { color, set } from 'react-native-reanimated';
import { captureScreen } from "react-native-view-shot";//1
const firestoreDB = firebase.firestore();

//We are Proud to carry out the records of our valued member in the world-wide fast growing Futsal Community
//import { auth } from 'firebase';

const { width, height } = Dimensions.get('screen');
const thumbMeasure = (width - 48 - 32) / 3;
const authFire=firebase.auth();
const user=authFire.currentUser;
const colref=firebase.firestore().collection('player'); 
const storage = firebase.storage();
console.disableYellowBox = true;

import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import uuid from 'uuid';
import { render } from 'react-dom';

const showToastWithGravityAndOffset = () => {
  ToastAndroid.showWithGravityAndOffset(
    "Successfully Updated!",
    ToastAndroid.LONG,
    ToastAndroid.TOP,
    25,
    height/5
  );
};


export default class ShareSS extends React.Component {

  constructor(props) {
    
    super(props);

    this.state = {
      username: "",
      mobile_number:"",
      email:"",
      user_data: [],
      screenshot:null,
      image: null,
      local_addr:null,
      uploading: false,
      }
    this.SSWithDelay();
     // this.post_share();
    }

    takeScreenShot=()=>{//2
      //handler to take screnshot
      captureScreen({
        //either png or jpg or webm (Android). Defaults to png
        format: "jpg",
        //quality 0.0 - 1.0 (default). (only available on lossy formats like jpg)
        quality: 0.8,
        width:width,
        height:height,
        
      })
      .then(
        //this._handleImagePicked(pickerResult)
        //callback function to get the result URL of the screnshot
        uri => this._handleImagePicked(uri),
        error => console.error("Oops, Something Went Wrong", error),
        //this.post_share(this.state.screenshot)
      ).catch(error=>{
        Alert.alert("Sorry! You're offline")
      });
      //console.log("takess",this.state.screenshot);
    }
    
    
    SSWithDelay=()=>{
      setTimeout(this.takeScreenShot, 2000);
    }


    
    post_share() {
      console.log("local add : ",this.state.local_addr)
      Share.share({
        message:"I'm in the world's fastest growing Futsal community which accompanied in my long journey :) Look my progress from here!\n \n"+this.state.screenshot+"",
        url: this.state.local_addr,
        title: 'My Futsal journey with SportsZone!'
      }, {
        // Android only:
        dialogTitle: 'Share Futsal Journey',
        // iOS only:
        excludedActivityTypes: [
          'com.apple.UIKit.activity.PostToTwitter'
        ]
      }).then(this.props.navigation.goBack(),
              console.log("went back")).catch(error=>{
                Alert.alert("Sorry! You're offline")
              });
    }
    

async componentDidMount() {
  var usermail=firebase.auth().currentUser.email.toString();
  
  await colref
  .doc(usermail)
    .onSnapshot(doc => {
      this.setState(
        { username:doc.data().username,
          mobile_number:doc.data().mobile,
          email:doc.data().email,
          image:doc.data().imageURL,
          screenshot:doc.data().screenshot,
          user_data: doc.data()}
    );
    //console.log(this.state.user_data);
    }).catch(error=>{
      Alert.alert("Sorry! You're offline")
    });
    await Permissions.askAsync(Permissions.CAMERA_ROLL);
    await Permissions.askAsync(Permissions.CAMERA);
    
  }
  

  render(){
    let { image } = this.state;
    const { user_data } = this.state;
    console.log("in renderrr : ",user_data)
    var total_played=parseInt(user_data.wins)+parseInt(user_data.loss)
    var wins_count=parseInt(user_data.wins);
    console.log("total played : ",total_played);
    var update_pressed=false;
    return(
      <Block flex style={styles.profile}  backgroundColor='#256e2e'  >
      <ScrollView >
        <Block flex  >
          <ImageBackground 
          backgroundColor='white'
            source={require('../assets/images/futsalbg.png')}
            style={styles.profileContainer}
            resizeMode="cover"//
            //height={height}
            //width={width}
            >
              <Block  row middle   >
              
                    <Avatar
                      rounded
                      size="xlarge"
                      source={{
                        uri:
                          this.state.image,
                      }}   
                      marginTop={10}
                      borderColor='black'
                    />
                    
              </Block>
              <Block  >
              
                <Text style={styles.ssdesc} >-Thanks for being a Valued Member of the Fastest Growing Futsal Community-</Text>
              
              </Block>
            

          </ImageBackground>
        </Block>
        <Block flex style={styles.options} backgroundColor="#36ad71" >
          <Text style={styles.titleText}>User Name</Text>
          <Input
              placeholder={user_data.username}
              right
              icon="heart"
              family="antdesign"
              iconSize={14}
              iconColor="green"
              placeholderTextColor="green"
              color="green"

          />
          <Text style={styles.titleText}>Email</Text>
          <Input
            placeholder={user_data.email}
            //style={{ borderColor: "blue" }}
            //placeholderTextColor="green"
            placeholderTextColor="green"

          />
          <Text style={styles.titleText}>Matches Played</Text>
          <Input
            placeholder={total_played.toString()}
            //style={{ borderColor: "blue" }}
            placeholderTextColor="green"

          />
          
          <Text style={styles.titleText}>Wins</Text>
          <Input
            placeholder={wins_count.toString()}
            //style={{ borderColor: "blue" }}
            placeholderTextColor="green"
          />
        </Block>
        <Button style={styles.btn} round size='large' onPress={()=>this.post_share()} color="#396ca3"><Text style={styles.baseText}>Share</Text></Button>
        <Button style={styles.btn} round size='large' onPress={()=>this.props.navigation.goBack()} color="#04d690"><Text style={styles.baseText}>Go Back</Text></Button>
      
        </ScrollView>
        <Button style={styles.btn} round alignSelf='center' size='large' color="#fdb940"><Text style={styles.baseText} >Congratulations {this.state.username}!</Text></Button>
        
      </Block>
    );
  }

  
  _maybeRenderUploadingOverlay = () => {
    if (this.state.uploading) {
      return (
        <View
          style={[
            StyleSheet.absoluteFill,
            {
              backgroundColor: 'rgba(0,0,0,0.4)',
              alignItems: 'center',
              justifyContent: 'center',
            },
          ]}>
        </View>
      );
    }
  };

  _maybeRenderImage = () => {
    let { screenshot } = this.state;
    if (!screenshot) {
      return;
    }

    return (
      <View
        style={{
          marginTop: 30,
          width: 250,
          borderRadius: 3,
          elevation: 2,
        }}>
        <View
          style={{
            borderTopRightRadius: 3,
            borderTopLeftRadius: 3,
            shadowColor: 'rgba(0,0,0,1)',
            shadowOpacity: 0.2,
            shadowOffset: { width: 4, height: 4 },
            shadowRadius: 5,
            overflow: 'hidden',
          }}>
          <Image source={{ uri: screenshot }} style={{ width: 250, height: 250 }}  />
        </View>
        <Text
          onPress={this._copyToClipboard}
          onLongPress={this._share}
          style={{ paddingVertical: 10, paddingHorizontal: 10 }}>
          {screenshot}
        </Text>
      </View>
    );
  };

  _share = () => {
    Share.share({
      message: this.state.screenshot,
      title: 'Check out this photo',
      url: this.state.screenshot,
    }).catch(error=>{
      Alert.alert("Sorry! You're offline")
    });
  };

  _copyToClipboard = () => {
    Clipboard.setString(this.state.screenshot);
    alert('Copied image URL to clipboard');
  };

  _takePhoto = async () => {
    let pickerResult = await ImagePicker.launchCameraAsync({
      allowsEditing: true,
      aspect: [4, 3],
    });

    this._handleImagePicked(pickerResult);
  };


  _handleImagePicked = async pickerResultURI => {
    this.state.local_addr=pickerResultURI;
    try {
      this.setState({ uploading: true });

     // if (!pickerResult.cancelled) {
        const uploadUrl = await uploadImageAsync(pickerResultURI);
        this.setState({ screenshot: uploadUrl });
     // }
    } catch (e) {
      console.log(e);
      alert('Upload failed, sorry :(');
    } finally {
      this.setState({ uploading: false });
    }
    colref.doc(firebase.auth().currentUser.email.toString()).update({
      screenshot:this.state.screenshot
    }).catch(error=>{
      Alert.alert("Sorry! You're offline")
    });
    
  };
  
}

async function uploadImageAsync(uri) {
  // Why are we using XMLHttpRequest? See:
  // https://github.com/expo/expo/issues/2402#issuecomment-443726662
  const blob = await new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.onload = function() {
      resolve(xhr.response);
    };
    xhr.onerror = function(e) {
      console.log(e);
      reject(new TypeError('Network request failed'));
    };
    xhr.responseType = 'blob';
    xhr.open('GET', uri, true);
    xhr.send(null);
  });

  const ref = firebase
    .storage()
    .ref('players/socialshare')
    .child(uuid.v4());
  const snapshot = await ref.put(blob);

  // We're done with the blob, close and release it
  blob.close();

  return await snapshot.ref.getDownloadURL();

}


const styles = StyleSheet.create({
  baseText: {
    fontSize:20,
    color:'white',
    fontWeight: "bold"
  },
  titleText: {
    fontSize: 18,
    fontWeight: "bold"
  },
  ssdesc: {
    paddingTop:height/50,
    fontSize: 15,
    fontWeight: "bold",
    color:'#13193d',
    paddingLeft:width/15,
    paddingRight:width/15,
    textAlign:"center"
   // backgroundColor:'#91d611'
  },
  profile: {
    //paddingTop:0,
   // marginTop:  -HeaderHeight/2,
   height:height*0.8,
   alignSelf:"center"
  },
  
  btn:{
   // opacity:0.7,
    alignSelf:"center",
    width:width*0.9,

  },

  profileContainer: {
    marginTop:HeaderHeight/1.5,
    width: width,
    height: height*0.96 ,
    flexDirection: "column",
    position:"relative",
    //alignSelf:"center"
  },

  profileTexts: {
    paddingBottom:0,
    paddingTop:0,
    zIndex: 2,
    alignItems:"center"

  },
  options: {
    position: 'relative',
    padding: theme.SIZES.BASE,
    marginHorizontal: theme.SIZES.BASE,
    marginTop: -theme.SIZES.BASE * 32,
    borderTopLeftRadius: 13,
    borderTopRightRadius: 13,
    borderBottomLeftRadius:13,
    borderBottomRightRadius:13,
    backgroundColor: theme.COLORS.WHITE,
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 8,
    shadowOpacity: 0.2,
    zIndex: 2,
  },
  thumb: {
    borderRadius: 4,
    marginVertical: 4,
    alignSelf: 'center',
    width: thumbMeasure,
    height: thumbMeasure
  },
});
