import React from 'react';
import { StyleSheet, Dimensions, ScrollView, FlatList,View,Image,TouchableOpacity } from 'react-native';
import { Button, Block, Text, Input, theme } from 'galio-framework';
import { AntDesign } from '@expo/vector-icons'; 
 
import firebase from '../firebase';
import { Icon, Product } from '../components';
import Theme from '../constants/Theme';

const { width } = Dimensions.get('screen');



export default class Home extends React.Component {
  state = {
    data: [],
    
  };

componentDidMount () {
    
      const {teamname} = this.props.route.params;
      const user = firebase.auth().currentUser.email;
      const list = [];
      firebase.firestore().collection("player").where('current_teams', 'array-contains',
      teamname).onSnapshot((querySnapshot) => {
      querySnapshot.forEach(doc => {
        
        if ('current_teams', 'array-contains',
        teamname){
          const { username,imageURL} = doc.data();
          console.log('if done');
          if(imageURL == null){
            list.push({
              id: doc.id,
              title: username,
              logo:'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT3BA8Cz7zVmsHKaVMLhpaL1LwnwFClTjXgMg&usqp=CAU',
              
          });
          }else{
            list.push({
              id: doc.id,
              title: username,
              logo:imageURL,
              
          });
          }
          
          }
      });

      this.setState({data: list});
      console.log(this.state.data);
  });
  
}

deletePress(id_num){
  console.log("Pressed");
  const {navigation} = this.props;
  const {teamname} = this.props.route.params;
  const teams= firebase.firestore().collection('team');
  const players= firebase.firestore().collection('player');
  const user = firebase.auth().currentUser.email;
  teams.where("teamName", "==", teamname)
  .get()
  .then(function(querySnapshot) {
      console.log("query ran");
      querySnapshot.forEach(function(doc) {
        console.log("forEach")
        console.log(doc.id);
        teams.doc(doc.id).update( {
          currentPlayers: firebase.firestore.FieldValue.arrayRemove( id_num )
       });

      });
  })
  .catch(function(error) {
      console.log("Error getting documents: ", error);
  });
   
  
  // navigation.navigate('PlayerProfile',{title: title, picture: logo, id:id_num});
  players.where("email", "==", id_num)
  .get()
  .then(function(querySnapshot) {
      console.log("query ran");
      querySnapshot.forEach(function(doc) {
        console.log("forEach")
        console.log(doc.id);
        players.doc(doc.id).update( {
          current_teams: firebase.firestore.FieldValue.arrayRemove( teamname)
       });
       
       

      });
  })
  .catch(function(error) {
      console.log("Error getting documents: ", error);
  });
  

}

  renderProducts = () => {
    const { navigation } = this.props;
    return (
     
      <FlatList
           data={this.state.data}
           renderItem={({item})=>
          
           <View style={styles.list }>
            
           <Image source = {{uri:item.logo}} style={styles.imageView} />
          {/* <View style={{flex:2, flexDirection: 'column',justifyContent:"center", alignItems:"center"}}>
              
          </View> */}
           
           <View style={{flex:4, flexDirection: 'column', alignItems:"flex-end",justifyContent:"center",paddingRight:120}}>
           <Text size={16}>{item.title}</Text>
           
           </View>
           <TouchableOpacity
                onPress={() => this.deletePress(item.id)}>
           <View style={{flex:4, flexDirection: 'column', alignItems:"flex-end",justifyContent:"center", paddingRight:10}}>
           <AntDesign name="deleteuser" size={24} color="red" />
           
           </View>
           </TouchableOpacity>
         </View>
         
         }
         keyExtractor={item => item.id}
         />


 )
    
  }

  render() {
    const { navigation } = this.props;
    return (
      
      <Block flex style={styles.home}>
        
        {/* <View style={{padding:20,flexDirection:"row",backgroundColor:"#32cd32", justifyContent:"center"}}>
          <Text style={{fontSize:30, color:"white"}}>Players</Text>
        </View> */}
        
        

        <View style={{padding:5}}></View>
        {this.renderProducts()}

        {/* <TouchableOpacity
          activeOpacity={0.7}
          onPress={()=>navigation.navigate('Add Player',{teamname:team_name})}
          style={styles.TouchableOpacityStyle}>
          <Icon name="plus-circle" family="material-community" size={55}
          color="#3BAD36" backgroundColor="#fff"
          />
        </TouchableOpacity> */}
        
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  home: {
    width: width,    
  },
  search: {
    height: 48,
    width: width - 32,
    marginHorizontal: 16,
    borderWidth: 1,
    borderRadius: 3,
  },
  header: {
    backgroundColor: theme.COLORS.WHITE,
    shadowColor: theme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 8,
    shadowOpacity: 0.2,
    elevation: 4,
    zIndex: 2,
  },
  tabs: {
    marginBottom: 24,
    marginTop: 10,
    elevation: 4,
  },
  tab: {
    backgroundColor: theme.COLORS.TRANSPARENT,
    width: width * 0.50,
    borderRadius: 0,
    borderWidth: 0,
    height: 24,
    elevation: 0,
  },
  tabTitle: {
    lineHeight: 19,
    fontWeight: '300'
  },
  divider: {
    borderRightWidth: 0.3,
    borderRightColor: theme.COLORS.MUTED,
  },
  products: {
  //  width: width - theme.SIZES.BASE * 2,
  //  paddingVertical: theme.SIZES.BASE * 2,
    flex:1,
    flexDirection:"row",
  },
  TouchableOpacityStyle: {
    //Here is the trick
    position: 'absolute',
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    right: 35,
    bottom: 35,
  },
  list:{
    flex:1, 
    flexDirection: 'row', 
    justifyContent:"space-evenly",
    backgroundColor:"#fff",
    borderBottomWidth:10,
    borderBottomColor:Theme.COLORS.BORDER_COLOR,
    padding:1,
    borderRadius:20
  },
  imageView: {
    width: width/5,
    height: width/5 ,
    margin: 7,
    borderRadius : 7,
},

});