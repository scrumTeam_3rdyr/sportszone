import React from 'react';
import { ImageBackground, StyleSheet, StatusBar, Dimensions, Platform, KeyboardAvoidingView, TouchableOpacity, Alert } from 'react-native';
import { Block, Button, Text, theme, Input } from 'galio-framework';
import firebase from '../firebase';
import Screens from '../navigation/Screens';
//import 'firebase/firestore'


//const playerCollection = firebase.firestore().collection('player');
const { height, width } = Dimensions.get('screen');

import materialTheme from '../constants/Theme';

function abc(username,mobile_number){
  console.log("abc",username);
  firebase.firestore().collection('player').doc(username).set({
    email: username,
    mobile: mobile_number,
    username:username,
    //gender:"",
    player:true,
    wins:0,
    loss:0,
    screenshot:"",
    imageURL:"https://firebasestorage.googleapis.com/v0/b/sportszone-fd46a.appspot.com/o/players%2Favatar.jpeg?alt=media&token=7f380ee1-b4fc-42f9-ba08-d9f12ef1f041"
  })
}
export default class SignupPlayer extends React.Component {

  constructor(props) {
    //console.log("constructor" ,usersCollection);
    super(props);
   // this.dbRef = firebase.firestore().collection('player');


    this.state = {
      mobile_number: "",
      username: "",
      password: "",
      error_mobile_number: "",
      error_username: "",
      error_password: "",
      error_confirm_password: "",
      error: "",
      confirm_password: "",
    }
  }

  empty_mobile_number_validator(){
    if(this.state.mobile_number==""){
      this.setState({error_mobile_number:"Please fill mobile number"})
    }else{
      this.setState({error_mobile_number:""})
    }
    }
  empty_username_validator(){
    if(this.state.username==""){
      this.setState({error_username:"Please fill email"})
    }else if(this.state.username){

    }else{
      this.setState({error_username:""})
    }
  }
  empty_password_validator(){
    if(this.state.password==""){
      this.setState({error_password:"Please fill password"})
    }else{
      this.setState({error_password:""})
    }
  }

  empty_confirm_password_validator(){
    if(this.state.confirm_password==""){
      this.setState({error_confirm_password:"Please fill confirm password"})
    }else{
      this.setState({error_confirm_password:""})
    }
  }
  password_validator(){
    if(this.state.password!=this.state.confirm_password){
      this.setState({error:"Passwords does not match"})
    }else{
      this.setState({error:""})
    }
  }
  validate_mail = (text) => {
    console.log(text);
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      console.log("Email is Not Correct");
      this.setState({ username: text })
      return false;
    }
    else {
      this.setState({ username: text })
      console.log("Email is Correct");
      return true;
    }
  }
  
  
  updateInputVal = (val, prop) => {
    console.log("update individual "+val);
    const state = this.state;
    state[prop] = val;
    this.setState(state);
    
  }
  handleSignUp = () => {
    const { username, password, mobile_number,confirm_password } = this.state
    /*const isregistered=firebase.auth().fetchSignInMethodsForEmail(username);
    if(isregistered){
      console.log("is registered",isregistered);
      Alert.alert("You email is already registered!");
      return;
    }*/

    var email_type_validity=this.validate_mail(username);
    console.log("valid email ? ",email_type_validity);
    if(!(username&&password&&mobile_number&&confirm_password)){
      Alert.alert("Please Fill Out The Empty Fields");
      return;
    }else if(!email_type_validity){
      Alert.alert("Please Enter a Valid Email Address");
      return;
    }else if(this.state.password!=this.state.confirm_password){
      Alert.alert("Passwords doesn't match");
      return;
    }
    else if(this.state.mobile_number.length<10){
      Alert.alert("please add a valid phone number");
    }
    /*if(mobile_number.length!='12'){
      Alert.alert("Please Enter a Valid Phone Number with Country Code");
      return;
    }
    else if(!this.state.error){
      console.log("error : ",this.state.error);
      Alert.alert(this.state.error);
    }*/
    else{
    console.log("error : ",this.state.error);
    firebase.auth()
        .createUserWithEmailAndPassword(username, password)
        .then(console.log("in then"))
        .catch(error =>{ Alert.alert("This email already has an account");
          console.log("in catch");
          return;
            })

    firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
        console.log("user in")
        Alert.alert("Successfully Registered! Please sign in to continue")
        abc(username,mobile_number)
        return (
          <Screens userState={'App'}/>
      );
        
      } else {
        // No user is signed in.
        console.log("user not")
        return (
          <Screens userState={'Login'}/>
      );
      }
    })
   // this.props.navigation.navigate('Login');

    /*  this.dbRef.doc(username).set({
        email: username,
        mobile: mobile_number,
        username:username,
        //gender:"",
        status:"free",//allocated or free...inintially free at current time
        wins:0,
        loss:0,
        screenshot:"",
        imageURL:"https://firebasestorage.googleapis.com/v0/b/sportszone-fd46a.appspot.com/o/players%2Fuser.png?alt=media&token=965ed479-0bdb-4901-a0a0-95cd89c06f68"
      })
      
      */
    }
  }



  render() {
    //console.log("render");
    const { navigation } = this.props;
    return (
      <Block style={styles.container}>
      <KeyboardAvoidingView behavior="height" enabled style={{flex:1}}>
        <StatusBar barStyle="light-content" />
          <Block row flex space="around" style={{ zIndex: 1 }}>
            <Block flex center>
              <Block>
                <Text color="black" size={60}>SignUp</Text>
              </Block>
              <Block>             
                <Input placeholder="Email" 
                  placeholderTextColor="black" 
                  minLenght={3} color="black" 
                  type='email'
                  style={styles.input}
                  value={this.state.username}
                  onChangeText={(val) => this.updateInputVal(val, 'username')}
                  onBlur={() => this.empty_username_validator()}
                  autoCapitalize='none'
                />
                <Text style={{ color: 'red', marginLeft: 20 }}>{this.state.error_username}</Text>

                <Input placeholder="Phone Number" 
                      minLenght={10} 
                      maxLength={10} 
                      placeholderTextColor="black" 
                      type="number-pad" 
                      color="black" 
                      style={styles.input}
                      value={this.state.mobile_number}
                      onChangeText={(val) => this.updateInputVal(val, 'mobile_number')}
                      onBlur={() => this.empty_mobile_number_validator()}
                />
                <Text style={{ color: 'red', marginLeft: 20 }}>{this.state.error_mobile_number}</Text>
              
                <Input placeholder="Password" 
                  password 
                  viewPass 
                  placeholderTextColor="black" 
                  color="black"
                  minLenght={6} color="black" 
                  iconColor="black" 
                  style={styles.input}
                  value={this.state.password}
                  onChangeText={(val) => this.updateInputVal(val, 'password')}
                  onBlur={() => this.empty_password_validator()}
                  //secureTextEntry={true}
                />
              <Text style={{ color: 'red', marginLeft: 20 }}>{this.state.error_password}</Text>

              <Input placeholder="Confirm Password" 
                password 
                viewPass 
                placeholderTextColor="black" 
                minLenght={6} color="black" 
                iconColor="black" 
                style={styles.input}              
                value={this.state.confirm_password}
                onChangeText={(val) => this.updateInputVal(val, 'confirm_password')}
                //secureTextEntry={true}
                //onBlur = {()=>this.empty_confirm_password_validator()}
                onBlur = {()=>this.password_validator()}
              />
              <Text style={{ color: 'red', marginLeft: 20 }}>{this.state.error}</Text>

            </Block>

            <Button
              shadowless
              style={styles.button}
              color={materialTheme.COLORS.GREEN}
              //onPress={() => this.validate_feild()}
              onPress={() => this.handleSignUp()}
            //onPress={console.log("in button click")}
            //onPress={() => navigation.navigate('App')}
            >
              SignUp
            </Button>

            <Block row>
              <Text color="black"> Do you already have an account? </Text>
              <TouchableOpacity
                onPress={() => navigation.navigate('Login')}>
                <Text color={materialTheme.COLORS.GREEN} style={{ fontWeight: "bold" }}>Login</Text>
              </TouchableOpacity>
            </Block>
          </Block>
        </Block>
      </KeyboardAvoidingView>
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  padded: {
    paddingHorizontal: theme.SIZES.BASE * 1,
    position: 'relative',
    bottom: theme.SIZES.BASE * 3,
  },
  button: {
    width: width - theme.SIZES.BASE * 10,
    height: theme.SIZES.BASE * 3,
    shadowRadius: 0,
    shadowOpacity: 0,
  },
  input: {
    width: width - theme.SIZES.BASE * 4,
    height: theme.SIZES.BASE * 3,

  },
  input: {
    width: width - theme.SIZES.BASE * 4,
    height: theme.SIZES.BASE * 3,
    borderColor: "white",
    borderBottomColor: "black",
    backgroundColor: "white"
  },
});

