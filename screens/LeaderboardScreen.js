import React,{useState,useEffect} from 'react';
import { StyleSheet, Dimensions, ScrollView,FlatList } from 'react-native';
import { Button, Block, Text, Input, theme } from 'galio-framework';

import LeaderboardItem from '../components/LeaderboardItem';
import { scores } from '../Data/DummyData';
import firebase from '../firebase';
import Leaderboard from '../Models/leaderboard';

const getTeamReaults = async (court) => {
    const db = firebase.firestore()
   
    const ref = db.collection("court").doc(court).collection('result');

    const resultSnapshot = await ref.get();

    const results = resultSnapshot.docs.map(resultSnapshot => {
        return resultSnapshot.data();
    })
    const loadedresults = [];
    for (const key in results) {// noway to filter dates
        loadedresults.push(new Leaderboard(
            results[key].teamId,
            results[key].teamName,
            results[key].wins,
            results[key].losses,
            results[key].matchCount,
            results[key].points
        ));
    }
    return loadedresults;




}



const LeaderboardScreen = props =>{

    const [resultData, setResultData] = useState([]);
    const courtEmail = props.route.params.email;
    useEffect(() => {
        const getResultData = async () => {
            const rdata = await getTeamReaults(courtEmail);
            setResultData(rdata);
        };
        getResultData();

    }, [setResultData]);
   
    console.log("sorted data",resultData.sort((a,b)=>(a.points < b.points)? 1 : -1));
    let i = 0;
    const renderScores = itemData => {
         i= i+1;
        return (
            <LeaderboardItem
                index = {i} 
                teamName ={itemData.item.teamId}
                teamName={itemData.item.teamName}
                wins={itemData.item.wins}
                losses={itemData.item.losses}
                matchCount={itemData.item.matchCount}
                points ={itemData.item.points}
               
            />
        );
    };
    return(
        <Block style ={styles.screen}>
            <Block style ={styles.header}>
            <Block style={styles.indexContainer}>
                <Text style={styles.index}>#</Text>
            </Block>
            <Block style={styles.teamNameContainer}>
                <Text style={styles.teamName}>Team</Text>
            </Block>
            <Block style={styles.winsContainer}>
                <Text style={styles.wins}>W</Text>
            </Block>
            <Block style={styles.lossesContainer}>
                <Text style={styles.losses}>L</Text>
            </Block>
            <Block style={styles.matchCountContainer}>
                <Text style={styles.matchCount}>MP</Text>
            </Block>
            <Block style={styles.pointsContainer}>
                <Text style={styles.points}>p</Text>
            </Block>
            </Block>
            <FlatList
                data={resultData.sort((a,b)=>(a.points < b.points)? 1 : -1)}
                renderItem={renderScores}
            />

            {/* <Text>Leaderboard</Text>   */}
            {/* <Button onPress ={()=>{
                props.navigation.navigate('matchResult', { court: courtEmail})}}> result</Button> */}
        </Block>
    );
}

const styles = StyleSheet.create({

    screen:{
        flex:1,
        backgroundColor:'#dedfda'

    },

    header:{
        flexDirection: 'row',
        height: theme.SIZES.BASE *2.5,
        width: theme.SIZES.BASE * 23,
        backgroundColor:'#edf1e8',
        marginHorizontal: theme.SIZES.BASE *0.8 ,
        marginTop: theme.SIZES.BASE,
        
    },
    indexContainer: {
        width: theme.SIZES.BASE *2.5,
        //borderWidth:1,
        justifyContent:'center',
        alignItems:'center'
     },
     index: {
        fontSize:16,
         fontWeight:'bold'
     },
     teamNameContainer: {
         width: theme.SIZES.BASE *8.5,
       // borderWidth:1,
        justifyContent:'center',
        // alignItems:'center'
     },
     teamName: {
         fontSize:16,
         fontWeight:'bold'
     },
     winsContainer: {
         width: theme.SIZES.BASE *3,
        // borderWidth:1,
         justifyContent:'center',
        alignItems:'center'
     },
     wins: {
        fontSize:16,
         fontWeight:'bold',
         color:'green'
     },
     lossesContainer: {
         width: theme.SIZES.BASE *3,
        // borderWidth:1,
         justifyContent:'center',
        alignItems:'center'
     },
     losses: {
        fontSize:16,
         fontWeight:'bold',
         color:'red'
     },
     matchCountContainer: {
         width: theme.SIZES.BASE *3,
       //  borderWidth:1,
         justifyContent:'center',
        alignItems:'center'
     },
     matchCount: {
        fontSize:16,
         fontWeight:'bold',
         color:'#abab16'
     },
     pointsContainer: {
         width: theme.SIZES.BASE *3,
        // borderWidth:1,
         justifyContent:'center',
        alignItems:'center'
     },
     points: {
        fontSize:16,
         fontWeight:'bold',
         color:'blue'
     },
});

export default LeaderboardScreen;