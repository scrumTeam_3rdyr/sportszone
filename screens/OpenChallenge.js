import React,{useState, useEffect} from 'react';
import { StyleSheet, Dimensions, ScrollView, FlatList,View,Image,TouchableOpacity,Picker, Alert, RefreshControl } from 'react-native';
import { Button, Block, Text, Input, theme } from 'galio-framework';
import { Icon, Product } from '../components/';
import Theme from '../constants/Theme';
import moment from 'moment'
import firebase from '../firebase';
const { width } = Dimensions.get('screen');

const wait = (timeout) => {
  return new Promise(resolve => {
    setTimeout(resolve, timeout);
  });
}


export default function OpenChallenge(props) {

    const teams= firebase.firestore().collection('team');
    const matches=firebase.firestore().collection('matches')
    const [team,setTeam]= useState('null');
    const [data,setData]=useState([]);
    const [matchData,setMatchData]=useState([]);
    const courtRef = firebase.firestore().collection('court');
    const user = firebase.auth().currentUser.email;
    const [refreshing, setRefreshing] = React.useState(false);

//loading team list
  useEffect(()=>{
    let print;
    findteams().then((result)=>{
    print=result.map(function(val,index){
    if(val.data().imageUrl){
    return {'teamName' : val.data().teamName, 'id':val.id, "imageUrl":val.data().imageUrl} ;
    }
    else {
      return {'teamName' : val.data().teamName, 'id':val.id, "imageUrl":"https://firebasestorage.googleapis.com/v0/b/sportszone-fd46a.appspot.com/o/default-team-logo.jpg?alt=media&token=79a9e092-8723-4048-8db7-6a15ef16fb41"} ;
    }
    })
    setData(print);
  });
  
},[])
useEffect(()=>{
  findmatches().then((result)=>{
    let matchlist=result.map(function(val,index){
      return val.data();
    })
    console.log("Running ABC");
    setMatchData(matchlist);
  })
},[refreshing])

//refreshing matches list after updating teams
// useEffect(()=>{
//   if(team!='null'){
//     findmatches().then((result)=>{
//       let matchlist=result.map(function(val,index){
//         return val.data();
//       })
//       console.log("Running ABC");
//       setMatchData(matchlist);
//     })
//   }
//   },[])


const findteams= async ()=>{
  const querySnapshot = await teams.where('teamCaptain', '==', user).get();
  return querySnapshot.docs;
}
const findmatches=async ()=>{
  console.log(team);
  const querySnapshot = await matches.where('type', '==', 'open challenge').where('matchStatus','==','pending').get();
  console.log(querySnapshot.docs);
  return querySnapshot.docs;
}
 const renderTeam = () =>{
  // const [teamList, setTeamList] = useState([]);

  const teamList=data.map(function(val,index){
    //console.log(val.teamName)
    return <Picker.Item label={val.teamName} value={val}/>
  });
//hello
  return(
    <Picker
      selectedValue={team}
      itemStyle={{textAlign:"center"}}
      onValueChange={(itemValue, itemIndex) =>{
          console.log(itemIndex + " "+ itemValue);
          setTeam(itemValue);
        }
      }>
      <Picker.Item label="Select Team" value="null"/>
      {teamList}
  </Picker>    
    
  )
 }
  const acceptMatch=(match)=> {  
    Alert.alert(  
        'Accept Match',  
        `Accept Open Challenge from ${match.opponentTeamName} on ${match.date} ${match.time}`,  
        [  
            {  
                text: 'NO',  
                onPress: () => console.log('Cancel Pressed'),  
                style: 'cancel',  
            },  
            {text: 'YES', onPress: async () => {
              if(team=="null"){
                errorAlert();
              }
              else{
              const dateObj= moment(match.date,"MMM Do YY").format("DDMMMYYYY");
              console.log(dateObj);
              let available=false;
              let notavailable = await courtRef.doc(match.courtEmail).collection('availabletimeslots').doc(dateObj).collection('timeslots').doc(match.time).get()
              .then(function(doc){
                console.log("in here")
                if (doc.exists) {
                  console.log("Document data:", doc.data());
              } else {
                  // doc.data() will be undefined in this case
                  available=true
                  console.log("No such document!");
              }
              }).catch((err)=>{
                //available=true
                console.log(err)
              });
              console.log(available)
              if(available){
                  await matches.doc(match.requestId).update({matchStatus:"accepted", teamId:team.id,teamLogoUrl:team.imageUrl,teamName:team.teamName}).then(async ()=>{
                  await courtRef.doc(match.courtEmail).collection("availabletimeslots").doc(dateObj).collection("timeslots").doc(match.time)
                  .set({ booked:true,open:false,team1:team.id, team2:match.opponentTeamId }).then(async()=>{
                     let matchRef=courtRef.doc(match.courtEmail).collection("matches").doc();
                     let courtMatch=await matchRef.add({ id:matchRef.id,winner:"",isResultsUpdated:false,date: match.date,time_slot:match.time,team1Id:team.id,team1ImageUrl:team.imageUrl, team1Name:team.teamName,team2Id:match.opponentTeamId,team2ImageUrl:match.opponentTeamLogoUrl,team2Name:match.opponentTeamName })
                  });
                    }).catch(err=>console.log(err))
              }
            }
          }
          },  
        ]  
    );  
  }  
  const errorAlert =()=>{
    Alert.alert(
      "Select Team",
      "Please select the correct team for accepting match",
      [
        {text:'OK', onPress:()=>console.log("OK Pressed"),style:'cancel'}
      ]
    )
  }
  const renderMatches = () => {
    return (
     
      <FlatList
           data={matchData}
           renderItem={({item})=>
           <View style={styles.list}>
 
           <Image source = {{uri:item.opponentTeamLogoUrl}} style={styles.imageView} />
          <View style={{flex:2, flexDirection: 'column',justifyContent:"center"}}>
              <Text size={16}>{item.opponentTeamName}</Text>
          </View>
           
           <View style={{flex:4, flexDirection: 'column', alignItems:"flex-end",justifyContent:"center", paddingRight:10}}>
           <Text>{item.time}</Text>
           <Text >{item.date}</Text>
           <Text>{item.courtName}</Text>
           </View>
           <View style={{flex:2, flexDirection: 'column', justifyContent:"center" }}>
           <TouchableOpacity style={{alignSelf:"center"}} onPress={()=>{
             if(team!='null'){console.log(team); acceptMatch(item)}
             else{errorAlert()}}}>
             <Icon
              name='trophy'
              family='material-community'
              color='green'
              size={30}
            />  
          </TouchableOpacity>
           
           </View>
           {/* <View style={{flex:1, flexDirection: 'column', justifyContent:"center" }}>
           <TouchableOpacity>
             <Icon
              name='close'
              family='FontAwesome5'
              color='red'
            />  
          </TouchableOpacity>
           
           </View> */}

         </View>
         }
         keyExtractor={item => item.id}
         />


 )
    
  }
  const onRefresh = React.useCallback(() => {
    setRefreshing(true);

    wait(1000).then(() => setRefreshing(false));
  }, []);

    return (
      <Block flex style={styles.home}>
        <View style={styles.head}>
          
          <Image source = {{uri:"https://i.ya-webdesign.com/images/trophy-vector-png-4.png"}} style={styles.imageViewHead} />
          <Text style={styles.headText}>OPEN CHALLENGES</Text>
        </View>
        {renderTeam()}
        <ScrollView refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefresh} />}>
        {renderMatches()}
        </ScrollView>
      </Block>
    );
  
}

const styles = StyleSheet.create({
  home: {
    width: width,    
  },
  search: {
    height: 48,
    width: width - 32,
    marginHorizontal: 16,
    borderWidth: 1,
    borderRadius: 3,
  },
  header: {
    backgroundColor: theme.COLORS.WHITE,
    shadowColor: theme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 8,
    shadowOpacity: 0.2,
    elevation: 4,
    zIndex: 2,
  },
  tabs: {
    marginBottom: 24,
    marginTop: 10,
    elevation: 4,
  },
  tab: {
    backgroundColor: theme.COLORS.TRANSPARENT,
    width: width * 0.50,
    borderRadius: 0,
    borderWidth: 0,
    height: 24,
    elevation: 0,
  },
  tabTitle: {
    lineHeight: 19,
    fontWeight: '300'
  },
  divider: {
    borderRightWidth: 0.3,
    borderRightColor: theme.COLORS.MUTED,
  },
  products: {
  //  width: width - theme.SIZES.BASE * 2,
  //  paddingVertical: theme.SIZES.BASE * 2,
    flex:1,
    flexDirection:"row",
  },
  list:{
    flex:1, 
    flexDirection: 'row', 
    justifyContent:"space-evenly",
    backgroundColor:"#fff",
    //borderRadius:25,
    borderBottomWidth:10,
    borderBottomColor:Theme.COLORS.BORDER_COLOR,
    paddingVertical:10,
  },
  imageView: {
    width: width/5,
    height: width/5 ,
    margin: 7,
    borderRadius : 7,
},
head:{
  width:width,
  height:width/3,
  backgroundColor:"#3BAD36",
  justifyContent:"center",
},
headText:{
  alignSelf:"center",
  fontSize:20,
  color:"white",
  
},
imageViewHead:{
  width: width/5,
    height: width/5 ,
    margin: 7,
    borderRadius : 7,
    alignSelf:"center"
}
});
