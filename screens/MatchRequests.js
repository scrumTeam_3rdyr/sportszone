import React,{useState, useEffect} from 'react';
import { StyleSheet, Dimensions, ScrollView, FlatList,View,Image,TouchableOpacity,Picker, Alert, ImageComponent, RefreshControl } from 'react-native';
import { Button, Block, Text, Input, theme } from 'galio-framework';
import { Icon, Product } from '../components/';
import Theme from '../constants/Theme';
import moment from 'moment';
import firebase from '../firebase';
const { width } = Dimensions.get('screen');

const wait = (timeout) => {
  return new Promise(resolve => {
    setTimeout(resolve, timeout);
  });
}

function MatchRequests(props){
  
    const teams= firebase.firestore().collection('team');
    const matches=firebase.firestore().collection('matches');
    const courtRef = firebase.firestore().collection('court');
    const [team,setTeam]= useState('null');
    const [data,setData]=useState([]);
    const [matchData,setMatchData]=useState([]);
    const [refreshing, setRefreshing] = React.useState(false);
    const user =firebase.auth().currentUser.email;


//loading team list
  useEffect(()=>{
    let print;
    findteams().then((result)=>{
    print=result.map(function(val,index){
    return {'teamName' : val.data().teamName, 'id':val.id} ;
    })
    setData(print);
  });
},[])

//refreshing matches list after updating teams
useEffect(()=>{
  if(team!='null'){
    findmatches().then((result)=>{
      let matchlist=result.map(function(val,index){
        return val.data();
      })
      console.log("refreshing matchlist",matchlist);
      setMatchData(matchlist);
    })
  }
  },[team,refreshing])


const findteams= async ()=>{
  const querySnapshot = await teams.where('teamCaptain', '==', user).get();
  return querySnapshot.docs;
}
const findmatches=async ()=>{
  console.log("The team is",team);
  const querySnapshot = await matches.where('teamId', '==', team).where('matchStatus','==','pending').get();
 // console.log(querySnapshot.docs);
  return querySnapshot.docs;
}
 const renderTeam = () =>{
  // const [teamList, setTeamList] = useState([]);

  const teamList=data.map(function(val,index){
    //console.log(val.teamName)
    return <Picker.Item label={val.teamName} value={val.id}/>
  });
//hello
  return(
    <Picker
      selectedValue={team}
      itemStyle={{textAlign:"center"}}
      onValueChange={(itemValue, itemIndex) =>{
          console.log(itemIndex + " "+ itemValue);
          setTeam(itemValue);
        }
      }>
      <Picker.Item label="Select Team" value="null"/>
      {teamList}
  </Picker>    
    
  )
 }
 const acceptMatch=(match)=> {  
  Alert.alert(  
      'Accept Match',  
      `Accept match on ${match.date} ${match.time} against ${match.opponentTeamName} `,  
      [  
          {  
              text: 'NO',  
              onPress: () => console.log('Cancel Pressed'),  
              style: 'cancel',  
          },  
          {text: 'YES', onPress: async () => {
            console.log(match);
            const dateObj= moment(match.date,"MMM Do YY").format("DDMMMYYYY");
            console.log(dateObj);
            let available=false;
            
            let notavailable = await courtRef.doc(match.courtEmail).collection('availabletimeslots').doc(dateObj).collection('timeslots').doc(match.time).get()
            .then(function(doc){
              console.log("check")
              if (doc.exists) {
                console.log("Document data:", doc.data());
            } else {
                // doc.data() will be undefined in this case
                available=true
                console.log("No such document!");
            }
            }).catch((err)=>{
              //available=true
              console.log(err)
            });
            console.log(available)
            if(available){
            await matches.doc(match.requestId).update({matchStatus:"accepted"}).then(async ()=>{
            await courtRef.doc(match.courtEmail).collection("availabletimeslots").doc(dateObj).collection("timeslots").doc(match.time)
           .set({ booked:true,open:false,team1:match.teamId, team2:match.opponentTeamId, player:user }).then( async ()=>{
            let matchRef= courtRef.doc(match.courtEmail).collection("matches").doc()
            let matchCourtCol=await matchRef.set({id:matchRef.id,winner:"",isResultsUpdated:false, date: match.date,time_slot:match.time,team1Id:team.id,team1ImageUrl:team.imageUrl, team1Name:team.teamName,team2Id:match.opponentTeamId,team2ImageUrl:match.opponentTeamLogoUrl,team2Name:match.opponentTeamName })
           });
            })
          }
          }
          },  
      ]  
  );  
}  
const rejectMatch=(match)=> {  
  Alert.alert(  
      'Reject Match',  
      `Reject match on ${match.date} ${match.time} against ${match.opponentTeamName} `,  
      [  
          {  
              text: 'NO',  
              onPress: () => console.log('Cancel Pressed'),  
              style: 'cancel',  
          },  
          {text: 'YES', onPress: () => matches.doc(match.requestId).update({matchStatus:"rejected"})},  
      ]  
  );  
} 
 const renderMatches = () => {
  return (
   
    <FlatList
         data={matchData}
         renderItem={({item})=>
         <View style={styles.list}>
         <Image source = {{uri:item.opponentTeamLogoUrl}} style={styles.imageView} />
        <View style={{flex:2, flexDirection: 'column',justifyContent:"center"}}>
            <Text size={16}>{item.opponentTeamName}</Text>
        </View> 
         <View style={{flex:4, flexDirection: 'column', alignItems:"flex-end",justifyContent:"center", paddingRight:10}}>
         <Text>{item.time}</Text>
         <Text >{item.date}</Text>
         <Text>{item.venue}</Text>
         </View>
         <View style={{flex:1, flexDirection: 'column', justifyContent:"center" }}>
         <TouchableOpacity onPress={()=>acceptMatch(item)}>
           <Icon
            name='check'
            family='FontAwesome5'
            color='green'
          />  
        </TouchableOpacity>
         </View>
         <View style={{flex:1, flexDirection: 'column', justifyContent:"center" }}>
         <TouchableOpacity onPress={()=>rejectMatch(item)}>
           <Icon
            name='close'
            family='FontAwesome5'
            color='red'
          />  
        </TouchableOpacity>
         </View>
       </View>
       }
       keyExtractor={item => item.requestId}
       />
)
}

    const onRefresh = React.useCallback(() => {
      setRefreshing(true);

      wait(1000).then(() => setRefreshing(false));
    }, []);

    const {navigation} = props;
    return (
      <Block flex style={styles.home}>
        {renderTeam()}
        <ScrollView refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />}>
        {renderMatches()}
        </ScrollView>
        <View>
        <TouchableOpacity
          //activeOpacity={0.7}
          onPress={()=>navigation.navigate('New Match')}
          style={styles.TouchableOpacityStyle}>
          <Icon name="plus-circle" family="material-community" size={55}
          color="#3BAD36" backgroundColor="#fff"
          />
        </TouchableOpacity>
        </View>
      </Block>
    );
    }

    export default MatchRequests;

const styles = StyleSheet.create({
  home: {
    width: width,    
  },
  search: {
    height: 48,
    width: width - 32,
    marginHorizontal: 16,
    borderWidth: 1,
    borderRadius: 3,
  },
  header: {
    backgroundColor: theme.COLORS.WHITE,
    shadowColor: theme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 8,
    shadowOpacity: 0.2,
    elevation: 4,
    zIndex: 2,
  },
  tabs: {
    marginBottom: 24,
    marginTop: 10,
    elevation: 4,
  },
  tab: {
    backgroundColor: theme.COLORS.TRANSPARENT,
    width: width * 0.50,
    borderRadius: 0,
    borderWidth: 0,
    height: 24,
    elevation: 0,
  },
  tabTitle: {
    lineHeight: 19,
    fontWeight: '300'
  },
  divider: {
    borderRightWidth: 0.3,
    borderRightColor: theme.COLORS.MUTED,
  },
  products: {
  //  width: width - theme.SIZES.BASE * 2,
  //  paddingVertical: theme.SIZES.BASE * 2,
    flex:1,
    flexDirection:"row",
  },
  list:{
    flex:1, 
    flexDirection: 'row', 
    justifyContent:"space-evenly",
    backgroundColor:"#fff",
    //borderRadius:25,
    borderBottomWidth:10,
    borderBottomColor:Theme.COLORS.BORDER_COLOR,
    paddingVertical:10,
  },
  imageView: {
    width: width/5,
    height: width/5 ,
    margin: 7,
    borderRadius : 7,
},
TouchableOpacityStyle: {
  //Here is the trick
  position: 'absolute',
  width: 50,
  height: 50,
  alignItems: 'center',
  justifyContent: 'center',
  right: 35,
  bottom: 35,
  backgroundColor:"white",
  borderRadius:40
},
FloatingButtonStyle: {
  resizeMode: 'contain',
  width: 50,
  height: 50,
  //backgroundColor:'black'
},

});
