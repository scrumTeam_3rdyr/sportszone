import React, { useState, useEffect, useCallback } from 'react';
import { StatusBar, StyleSheet, ImageBackground, Dimensions, FlatList, ActivityIndicator } from 'react-native';
import { Block, Text, theme, Button, Input } from 'galio-framework';
import { LinearGradient } from 'expo-linear-gradient';
import GuButton from '../components/Button';
import { Images, materialTheme } from '../constants';
import Icon from '../components/Icon';
import { SearchBar } from 'react-native-elements';
//import firebase from '../firebase';
//import 'firebase/firestore';


import Mybutton from '../components/MyButton';
import TeamItem from '../components/TeamItem';
import Team from '../Models/team';
import MatchRequest from '../Models/matchRequest';
import OpenChallenge from '../Models/openChallenge';
import { teams } from '../Data/DummyData';
import firebase from '../firebase';
const { width, height } = Dimensions.get('screen');



// const db = firebase.firestore();
// const ref = db.collection("team");
// console.log('ref',ref.get());
// const productSnapshot =  ref.get()
// const products = productSnapshot.docs.map(productSnapshot => {
//     return productSnapshot.data();
// })
// const loadedProducts = [];
// for (const key in products) {
//     //console.log('bbbbbbbbbb',products[key].id)
//     loadedProducts.push(new Team(
//         products[key].id,
//         products[key].teamName,
//         products[key].imageUrl,
//         products[key].current_players,
//         products[key].loss,
//         products[key].wins
//     ));
// }
// console.log('team',loadedProducts);


const getTeams = async (teamId) => {
    const db = firebase.firestore()

    const ref = db.collection("team");
    const teamsSnapshot = await ref.get()
    // console.log('ref',teamsSnapshot);
    const teams = teamsSnapshot.docs.map(teamSnapshot => {
        return teamSnapshot.data();
    })
    //  console.log("teams",teams);

    const loadedteams = [];
    for (const key in teams) {
        // console.log('bbbbbbbbbb', teams[key].id)
        if (teams[key].id != teamId) {
            loadedteams.push(new Team(
                teams[key].id,
                teams[key].teamName,
                teams[key].imageUrl,
                teams[key].currentPlayers,
                teams[key].teamCaptain,
                teams[key].loss,
                teams[key].wins
            ));
        };

    }
    return loadedteams;
}

const getAllTeams = async () => {
    const db = firebase.firestore()

    const ref = db.collection("team");
    const teamsSnapshot = await ref.get()
    // console.log('ref',teamsSnapshot);
    const teams = teamsSnapshot.docs.map(teamSnapshot => {
        return teamSnapshot.data();
    })
    //  console.log("teams",teams);

    const loadedteams = [];
    for (const key in teams) {
        // console.log('bbbbbbbbbb', teams[key].id)

        loadedteams.push(new Team(
            teams[key].id,
            teams[key].teamName,
            teams[key].imageUrl,
            teams[key].currentPlayers,
            teams[key].teamCaptain,
            teams[key].loss,
            teams[key].wins
        ));


    }
    return loadedteams;
}

const createMatchRequest = async (teamId,teamName,teamLogoUrl, opponentTeamId, opponentTeamName, opponentTeamLogoUrl, date, time, courtName,courtEmail, type, matchStatus) => {//playerId === captain id
    const db = firebase.firestore();
    const ref = db.collection('matches').doc();
    const request = new MatchRequest(ref.id, teamId,teamName,teamLogoUrl, opponentTeamId, opponentTeamName, opponentTeamLogoUrl, date, time, courtName,courtEmail, type, matchStatus);
    const rawRequest = JSON.parse(JSON.stringify(request));
    console.log('Creating new Request', request);
    await ref.set(rawRequest);
};

const createOpenChallenge = async (opponentTeamId, opponentTeamName, opponentTeamLogoUrl, date, time, courtName,courtEmail, type, matchStatus) => {//playerId === captain id
    const db = firebase.firestore();
    const ref = db.collection('matches').doc();
    const request = new OpenChallenge(ref.id, opponentTeamId, opponentTeamName, opponentTeamLogoUrl, date, time, courtName,courtEmail, type, matchStatus);
    const rawRequest = JSON.parse(JSON.stringify(request));
    console.log('Creating new Open Challenge', request);
    await ref.set(rawRequest);
};


const NewMatchSelectTeam = props => {

    // const team =  firestore().collection('team').get();

    // console.log(team);


    const [teamData, setTeamData] = useState([]);
    const [allTeamData, setAllTeamData] = useState([]);
    const [loadingTeamData, setLoadingTeamData] = useState(true);

    useEffect(() => {
        const getTeamData = async () => {
            setLoadingTeamData(true);
            const data = await getTeams(teamId);
            setTeamData(data);
            const allData = await getAllTeams();
            setAllTeamData(allData);
            setLoadingTeamData(false);

            updateSearch("");
            console.log("get dataa")
        };
        getTeamData();

    }, [setTeamData, setLoadingTeamData,updateSearch]);


    // useEffect(()=>{
    //     updateSearch("");
    // },[]);
    // if(loadingTeamData){
    //     return <Block style ={styles.centered} >
    //         <ActivityIndicator
    //             size ='large'
    //             // color = {Colors.primary}
    //         />
    //     </Block>
    // }


    const iconCamera = <Icon size={16} color={theme.COLORS.MUTED} name="zoom-in" family="material" />

    // const teamData = getTeams();

   // console.log("teams teams teams", teamData);
    const teamId = props.route.params.teamId;
    const court_name = props.route.params.courtName;
    const court_email = props.route.params.courtEmail;
    const date = props.route.params.date;
    const timeslot = props.route.params.timeslot;
    

    const sendRequestHandler = async () => {
        //const selectedTeam = teamData.find(team => team.id === id); //selectedTeam is Team/team that recieves the request
        const sendTeam = allTeamData.find(team => team.id === teamId);//sendTeam is  the team that sends the request / opponentTeam
        await createOpenChallenge(teamId, sendTeam.teamName, sendTeam.imageUrl, date, timeslot, court_name,court_email, 'open challenge', 'pending');
        
    };

    const sendRequestToOneTeamHandler = async (id) => {
        console.log(teamId);
        const selectedTeam = teamData.find(team => team.id === id); //selectedTeam is Team/team that recieves the request
        //const teamCaptain = selectedTeam.teamCaptain;
        const sendTeam = allTeamData.find(team => team.id === teamId);//sendTeam is  the team that sends the request / opponentTeam
        //console.log("send team",sendTeam,selectedTeam);
        await createMatchRequest(selectedTeam.id ,selectedTeam.teamName,selectedTeam.imageUrl,sendTeam.id, sendTeam.teamName, sendTeam.imageUrl, date, timeslot, court_name, court_email,'regular match', 'pending');
        //console.log("abc", selectedTeam);
        // const players = selectedTeam.currentPlayers
        //console.log("abc", players);

        // for (const key in players) {
        // console.log('Got the request', id, selectedTeam.teamCaptain);
        // }
        // props.navigation.navigate('MatchRequests', {
        //     teamName: team_name,
        //     courtName: court_name,
        //     date: date,
        //     time:time,

        // });
    };

    const [searchTeam, setSearchTeam] = useState('');
    const [data,setData] = useState([]);

    const updateSearch = useCallback((search) =>{
        
        setSearchTeam(search);
        //console.log("Sraet is ",{search,teamData});
        if(search === '') {
            setData(teamData);
        }
        else{
            const newData = teamData.filter(item =>{
                const itemData = `${item.teamName.toUpperCase()}`;
                const textData = search.toUpperCase();
                return itemData.indexOf(textData) > -1;
            });
           // console.log(newData);
            setData(newData);
        }
       // console.log("data is",data,teamData,loadingTeamData);
        

    },[data,teamData,setData,setSearchTeam])

    const renderTeams = itemData => {

        return (
            <TeamItem
                teamName={itemData.item.teamName}
                image={itemData.item.imageUrl}
                id={itemData.item.id}
                onRequest={() => { sendRequestToOneTeamHandler(itemData.item.id) }} />
        );
    };
    return (
        <Block flex style={styles.screen}>
            <Block flex>
                <ImageBackground
                    source={{ uri: Images.Openchallange }}
                    style={styles.profileContainer}
                    imageStyle={styles.profileImage}>
                    <Block flex style={styles.profileDetails}>
                        <Block style={styles.profileTexts}>

                            <Block row space="between">
                                <Text color="black" size={28} style={{ paddingBottom: 8 }}>OPEN CHALLENGE</Text>
                                <Mybutton onPress={sendRequestHandler} style={styles.button}>

                                </Mybutton>
                            </Block>
                            <Text size={16}> </Text>
                        </Block>
                        <LinearGradient colors={['rgba(0,0,0,0)', 'rgba(0,0,0,1)']} style={styles.gradient} />
                    </Block>
                </ImageBackground>
                <Block>
                    <Block style={styles.heading}>
                        <Text style={styles.headingText}>Challenge a team</Text>

                    </Block>
                    
                    <SearchBar
                        round
                        lightTheme
                        placeholder="Search opponent team....."
                        containerStyle={{ backgroundColor: theme.COLORS.TRANSPARENT , }}
                        inputContainerStyle={{ backgroundColor: theme.COLORS.WHITE }}
                        onChangeText={(search)=>updateSearch(search)}
                        //onClearText={someMethod}
                        value={searchTeam}
                    >

                    </SearchBar>
                    {/* {loadingTeamData ?
                    <Block style={styles.centered} >
                        <ActivityIndicator
                            size='large'
                        // color = {Colors.primary}
                        />
                    </Block> :
                    <FlatList
                        data={teamData}
                        renderItem={renderTeams}
                    />
                } */}
                    {/* <Input
                        right
                        color="black"
                        style={styles.search}
                        iconContent={iconCamera}
                        placeholder="Select a team"
                        // onChangeText={(val)=>{setEnteredTeam(val)}}
                        // value ={enteredTeam}
                        onFocus={() => { }}
                    /> */}
                </Block>
                {loadingTeamData ?
                    <Block style={styles.centered} >
                        <ActivityIndicator
                            size='large'
                        // color = {Colors.primary}
                        />
                    </Block> :
                    <FlatList
                        
                        data={searchTeam && searchTeam.length ? data: teamData}
                        renderItem={renderTeams}
                    />
                }
                {/* <FlatList
                        data={teamData}
                        renderItem={renderTeams}
                    /> */}

                {/* <Block style={styles.container}>

                   

                </Block> */}
            </Block>

        </Block>
    );
};
const styles = StyleSheet.create({
    screen: {
        flex: 1
    },
    profileImage: {
        width: width,
        height: 'auto',
    },
    profileContainer: {
        //marginTop: theme.SIZES.BASE,
        width: width - theme.SIZES.BASE / 20,
        height: height / 4,
    },
    profileDetails: {
        paddingTop: theme.SIZES.BASE * 4,
        justifyContent: 'flex-end',
        position: 'relative',
    },
    profileTexts: {
        paddingHorizontal: theme.SIZES.BASE * 7,
        paddingVertical: theme.SIZES.BASE * 4,
        zIndex: 2,
        fontWeight: 'bold'
    },
    button: {
        width: theme.SIZES.BASE * 5,
        height: theme.SIZES.BASE * 5,
        marginLeft: 10
    },
    heading: {
        marginHorizontal: 30,
        marginVertical: 10
    },
    headingText: {
        fontSize: 16,
        fontWeight: 'bold'
    },
    search: {
        height: 48,
        width: width - 32,
        marginHorizontal: 16,
        borderWidth: 1,
        borderRadius: 50,
        backgroundColor: 'transparent'
    },
    container: {

    },
    centered: {
        // flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});
export default NewMatchSelectTeam;