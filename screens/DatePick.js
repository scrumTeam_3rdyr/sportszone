import React, {useState} from 'react';
import {View, Button, Platform} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';
 

// React.useEffect(() => {
//     if (props.selectDate) {
//       props.selectDate(date)
//     }
//   }, [date])


const DatePick = (props) => {
  const [date, setDate] = useState(new Date().getTime());
  const [mode, setMode] = useState('date');
  const [show, setShow] = useState(false);
 
  
  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate.getTime() || date;
    setShow(Platform.OS === 'ios');
    setDate(currentDate);
    // wrapper.setProps({
    //   selectDate: currentDate,
    // })
    console.log("date pick " + currentDate);
    //console.log("props " + props.selectDate);
    props.selectDate(currentDate);
  };
 
  const showMode = (currentMode) => {
    setShow(true);
    setMode(currentMode);
  };
 
  const showDatepicker = () => {
    showMode('date');
  };
 
  const showTimepicker = () => {
    showMode('time');
  };
 
  return (
    <View>
      <View>
        <Button onPress={showDatepicker} title={moment(date).format("dddd MMM Do YYYY")} />
      </View>
      {/* <View>
        <Button onPress={showTimepicker} title="Show time picker!" />
      </View> */}
      {show && (
        <DateTimePicker
          testID="dateTimePicker"
          value={date}
          mode={mode}
          is24Hour={true}
          display="default"
          onChange={onChange}
        />
      )}
    </View>
  );
};
export default DatePick;