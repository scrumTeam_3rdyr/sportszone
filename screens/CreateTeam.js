import React, { useState, useReducer, useCallback, useEffect } from 'react';
import { StyleSheet, ScrollView, Dimensions, Alert, KeyboardAvoidingView, View} from 'react-native';
import { Block, Text, theme, Button } from 'galio-framework';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import { Avatar } from 'react-native-elements';
import materialTheme from '../constants/Theme';
import Input from '../components/Input';
import firebase from '../firebase';
import 'firebase/firestore';
const firestoreDB = firebase.firestore();

const teams= firebase.firestore().collection('team');
const { height, width } = Dimensions.get('screen');

const FORM_INPUT_UPDATE = 'FORM_INPUT_UPDATE';
const RESET_FORM = 'RESET_FORM';


const formReducer = (state, action) => {
    if (action.type === FORM_INPUT_UPDATE) {

        const updatedValues = {
            ...state.inputValues,
            [action.input]: action.value
        };
        
        const updatedValidations = {
            ...state.inputValidations,
            [action.input]: action.isValid
        };
        let updatedIsFormValid = true 
        for (const key in updatedValidations) {
            updatedIsFormValid = updatedIsFormValid && updatedValidations[key];
        }

        return {
            isFormValid: updatedIsFormValid,
            inputValues: updatedValues,
            inputValidations: updatedValidations
        }
    }
    if (action.type === RESET_FORM) {
        return {
            inputValues: {
                teamName: '',
                city: ''
            },
            inputValidations: {
                teamName: false,
                city: false
            },
            isFormValid: false
        };
    }
    return state;
};

const CreateTeam = props => {

    const [formState, dispatchFormState] = useReducer(formReducer, {
        inputValues: {
            teamName: '',
            city: '',
        },
        inputValidations: {
            teamName: false,
            city: false
        },
        isFormValid: false
    });

   

    const submitHandler = useCallback(() => {//avoid infinite loop by squre brackets
        
        if (!formState.isFormValid) {

            Alert.alert('Wrong input', 'Please check errors in the form!', [{ text: 'okay' }]);
            return;
        }
        const team_name = formState.inputValues.teamName
        const city = formState.inputValues.city
        const user = firebase.auth().currentUser.email;
        const players= firebase.firestore().collection('player');
        teams.add({
            teamName: team_name,
            city: city,
            teamCaptain: user
          });
          console.log(team_name + "team name before navigating");
          props.navigation.navigate('Add Player',{teamname:team_name});

      
       
        

    }, [formState]);

    const inputChangeHandler = useCallback((inputIdentifier, inputValue, inputValidity) => {

        dispatchFormState({
            type: FORM_INPUT_UPDATE,
            value: inputValue,
            isValid: inputValidity,
            input: inputIdentifier
        })
    }, [dispatchFormState]);


    


    return (
        <KeyboardAvoidingView behavior="padding" enabled style={styles.container}>
            <ScrollView>
            <View style={{padding:20}}></View>
              <Block  row middle  >
                      <Avatar
                        rounded
                        size="xlarge"
                        source={{
                          uri:
                            'https://cdn3.iconfinder.com/data/icons/glyph-background-coloured/614/22_-_Business_Team-512.png',
                        }}
                      />
                      
                </Block>
                <Block style={styles.form}>
                    <Input


                        id='teamName'
                        label='Team name:'
                        placeholder='Team name'
                        placeholderTextcolor = 'gray'
                        errorText='Please enter a valid Team name'
                        keyboardType='default'
                        autoCapitalize='sentences'
                        returnKeyType="next"
                        onInputChange={inputChangeHandler}
                        initialValue=''
                        initiallyValid={false}
                        required
                    />
                    <Input
                        id='city'
                        label='city:'
                        placeholder='city'
                        errorText='Please enter your city'
                        keyboardType='default'
                        autoCapitalize='sentences'
                        returnKeyType="next"
                        onInputChange={inputChangeHandler}
                        initialValue=''
                        initiallyValid={false}
                        required
                    />

                    
                    


                </Block>
                <Block style={styles.buttonContainer}>
                    <Button
                        shadowless
                        style={styles.button}
                        color='lightgreen'
                        round
                        textStyle ={styles.buttonText}
                        
                        onPress={submitHandler}
                        title="Create"
                    >Create</Button>

                </Block>

            </ScrollView>
        </KeyboardAvoidingView>
    );
};
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: theme.COLORS.WHITE
    },
    form: {
        flex: 1,
        alignContent: 'center',
        justifyContent: 'center',
        margin: theme.SIZES.BASE * 2,
        marginTop: theme.SIZES.BASE * 5
    },
    button: {
        width: width - theme.SIZES.BASE * 15,
        height: theme.SIZES.BASE * 3,
        shadowRadius: 0,
        shadowOpacity: 0,
    },
    buttonContainer: {
        marginLeft: theme.SIZES.BASE * 12
    },
    buttonText:{
        color:'black'
    }
});
export default CreateTeam;