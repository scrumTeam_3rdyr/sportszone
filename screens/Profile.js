import React,{ useState, useEffect } from 'react';
import { StyleSheet, Dimensions, ScrollView, Image, ImageBackground, Platform,View,ToastAndroid, Share, Alert } from 'react-native';
import { Block, Text, theme,Input,Toast,Button } from 'galio-framework';
import { HeaderHeight } from "../constants/utils";
import { Avatar ,Icon} from 'react-native-elements';
//firebase
import firebase from '../firebase';
import 'firebase/firestore';
import { color, set } from 'react-native-reanimated';
import { captureScreen } from "react-native-view-shot";//1
const firestoreDB = firebase.firestore();


//import { auth } from 'firebase';

const { width, height } = Dimensions.get('screen');
const thumbMeasure = (width - 48 - 32) / 3;
const colref=firebase.firestore().collection('player'); 
console.disableYellowBox = true;

import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import uuid from 'uuid';
import { render } from 'react-dom';

const showToastWithGravityAndOffset = () => {
  ToastAndroid.showWithGravityAndOffset(
    "Successfully Updated!",
    ToastAndroid.LONG,
    ToastAndroid.TOP,
    25,
    height/5
  );
};

export default class Profile extends React.Component {

  constructor(props) {
    
    super(props);

    this.state = {
      username: "",
      mobile_number:"",
      user_data: [],
      screenshot:null,
      image: null,
      uploading: false,
      }
    }

async componentDidMount() {
  var usermail=firebase.auth().currentUser.email.toString();
  await colref
  .doc(usermail)
    .onSnapshot(doc => {
      this.setState(
        { username:doc.data().username,
          mobile_number:doc.data().mobile,
          image:doc.data().imageURL,
          user_data: doc.data()}
    )
    //console.log(this.state.user_data);
    }).catch(error=>{
      Alert.alert("Sorry! You're offline")
    });
    await Permissions.askAsync(Permissions.CAMERA_ROLL);
    await Permissions.askAsync(Permissions.CAMERA);
  }
  
  updateInputVal = (val, prop) => {
    console.log("update individual"+val);
    const state = this.state;
    state[prop] = val;
    this.setState(state);
  }

  handleUpdate(){
    const { username, mobile_number } = this.state;
      colref.doc(firebase.auth().currentUser.email.toString()).update({
        mobile: mobile_number,
        username:username,

      })
      .then(()=>{showToastWithGravityAndOffset()})
      .catch(error=>{
        Alert.alert("Sorry! You're offline")
      });
  }

  viewOnPressed(){
    return (
          <Image source={{ uri: this.state.image }}/>
    );
  }

  render(){
    let { image } = this.state;
    const { user_data } = this.state;
    console.log("in renderrr : ",user_data)
    var total_played=parseInt(user_data.wins)+parseInt(user_data.loss)
    var wins_count=parseInt(user_data.wins);
    console.log("total played : ",total_played);
    var update_pressed=false;
    return(
      <Block flex style={styles.profile} >
      <ScrollView>
        <Block flex  >
          <ImageBackground 
            style={styles.profileContainer}
            >
            <Block flex  >
              <Block row middle   >
                    <Avatar 
                      rounded
                      backgroundColor="yellow"
                      size="xlarge"
                      source={{
                        uri:
                          this.state.image,
                      }}
                      onPress={() =>{this.viewOnPressed() }}               
                    />
              <Block>
              <Button style={styles.btn} onlyIcon onPress={() => {this._takePhoto()}} icon="camera" iconFamily="antdesign" iconSize={30} color="warning" iconColor="#fff" style={{ width: 40, height: 40 }}>warning</Button>
              <Button style={styles.btn} onlyIcon onPress={() => {this._pickImage()}} icon="upload" iconFamily="antdesign" iconSize={30} color="warning" iconColor="#fff" style={{ width: 40, height: 40 }}>warning</Button>
              </Block>
              </Block>

              <Block style={styles.profileTexts}  >
                <Text color="green" size={28} style={{ paddingTop: 25 }} >{firebase.auth().currentUser.email.toString()}</Text>
              </Block>

              
            </Block>

          </ImageBackground>
        </Block>
        <Block flex style={styles.options} backgroundColor="#2f994c" >
          <Text color='white'>User Name</Text>
          <Input
              placeholder={user_data.username}
              right
              icon="heart"
              family="antdesign"
              iconSize={14}
              iconColor="green"
              placeholderTextColor="green"
              color="green"
              value={this.state.username}
              onChangeText={(val) => this.updateInputVal(val, 'username')}
          

          />
          <Text color='white'>Mobile</Text>
          <Input
            placeholder={user_data.mobile}
            //style={{ borderColor: "blue" }}
            //placeholderTextColor="green"
            color="green"
            value={this.state.mobile_number}
            onChangeText={(val) => this.updateInputVal(val, 'mobile_number')}

          />
          
          <Text color='white'>Matches Played</Text>
          <Input
            placeholder={total_played.toString()}
            //style={{ borderColor: "blue" }}
            placeholderTextColor="green"
            value={this.state.matches}
            //onChangeText={(val) => this.updateInputVal(val, 'matches')}

          />
          <Text color='white'>Wins</Text>
          <Input
            placeholder={wins_count.toString()}
            //style={{ borderColor: "blue" }}
            placeholderTextColor="green"
            value={this.state.matches}
            //onChangeText={(val) => this.updateInputVal(val, 'wins')}
          />
        </Block>
        <Button style={styles.btn} round onPress={() => this.handleUpdate(this.state.username,this.state.mobile_number)} size='large' color="#2d83b3">Update Profile</Button>

        </ScrollView>
        <Button  style={styles.btn} round size='large' onPress={()=>this.props.navigation.navigate('ShareSS')} color="#4acf8a">Share My Progress!</Button>
    
      </Block>
    );
  }

  
  _maybeRenderUploadingOverlay = () => {
    if (this.state.uploading) {
      return (
        <View
          style={[
            StyleSheet.absoluteFill,
            {
              backgroundColor: 'rgba(0,0,0,0.4)',
              alignItems: 'center',
              justifyContent: 'center',
            },
          ]}>
        </View>
      );
    }
  };

  _maybeRenderImage = () => {
    let { image } = this.state;
    if (!image) {
      return;
    }

    return (
      <View
        style={{
          marginTop: 30,
          width: 250,
          borderRadius: 3,
          elevation: 2,
        }}>
        <View
          style={{
            borderTopRightRadius: 3,
            borderTopLeftRadius: 3,
            shadowColor: 'rgba(0,0,0,1)',
            shadowOpacity: 0.2,
            shadowOffset: { width: 4, height: 4 },
            shadowRadius: 5,
            overflow: 'hidden',
          }}>
          <Image source={{ uri: image }} style={{ width: 250, height: 250 }}  />
        </View>
        <Text
          onPress={this._copyToClipboard}
          onLongPress={this._share}
          style={{ paddingVertical: 10, paddingHorizontal: 10 }}>
          {image}
        </Text>
      </View>
    );
  };

  _share = () => {
    Share.share({
      message: this.state.image,
      title: 'Check out this photo',
      url: this.state.image,
    }).catch(error=>{
      Alert.alert("Sorry! You're offline")
      });
  };

  _copyToClipboard = () => {
    Clipboard.setString(this.state.image);
    alert('Copied image URL to clipboard');
  };

  _takePhoto = async () => {
    let pickerResult = await ImagePicker.launchCameraAsync({
      allowsEditing: true,
      aspect: [4, 3],
    });

    this._handleImagePicked(pickerResult);
  };

  _pickImage = async () => {
    let pickerResult = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3],
    });
    console.log("picker Result : ",pickerResult);
    this._handleImagePicked(pickerResult);
  };

  _handleImagePicked = async pickerResult => {
    try {
      this.setState({ uploading: true });

      if (!pickerResult.cancelled) {
        const uploadUrl = await uploadImageAsync(pickerResult.uri);
        this.setState({ image: uploadUrl });
      }
    } catch (e) {
      console.log(e);
      alert('Upload failed, sorry :(');
    } finally {
      this.setState({ uploading: false });
    }
    colref.doc(firebase.auth().currentUser.email.toString()).update({
      imageURL:this.state.image
    }).catch(error=>{
      Alert.alert("Sorry! You're offline")
    });
  };
  
}


async function uploadImageAsync(uri) {
  // Why are we using XMLHttpRequest? See:
  // https://github.com/expo/expo/issues/2402#issuecomment-443726662
  const blob = await new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.onload = function() {
      resolve(xhr.response);
    };
    xhr.onerror = function(e) {
      console.log(e);
      reject(new TypeError('Network request failed'));
    };
    xhr.responseType = 'blob';
    xhr.open('GET', uri, true);
    xhr.send(null);
  });

  const ref = firebase
    .storage()
    .ref('players/profile')
    .child(uuid.v4());
  const snapshot = await ref.put(blob);

  // We're done with the blob, close and release it
  blob.close();

  return await snapshot.ref.getDownloadURL();
  
}


const styles = StyleSheet.create({
  profile: {
    //paddingTop:0,
   // marginTop:  -HeaderHeight/2,
    alignSelf:"center"
  },
  btn:{
    // opacity:0.7,
    alignSelf:"center",
    width:width*0.9,
  },

  profileContainer: {
    marginTop:HeaderHeight,
    width: width,
    height: height ,
    alignSelf:"center"
  },

  profileTexts: {
    paddingLeft: width/10,
    paddingRight:width/10,
    paddingBottom:0,
    paddingTop:0,
    zIndex: 2,
    alignItems:"center"

  },
  avatar:{
   // imag:'../assets/images/avatar.jpeg'
  },
  options: {
    position: 'relative',
    padding: theme.SIZES.BASE,
    marginHorizontal: theme.SIZES.BASE,
    marginTop: -theme.SIZES.BASE * 32,
    borderTopLeftRadius: 13,
    borderTopRightRadius: 13,
    borderBottomLeftRadius:13,
    borderBottomRightRadius:13,
    backgroundColor: theme.COLORS.WHITE,
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 8,
    shadowOpacity: 0.2,
    zIndex: 2,
  },
  thumb: {
    borderRadius: 4,
    marginVertical: 4,
    alignSelf: 'center',
    width: thumbMeasure,
    height: thumbMeasure
  },
});
