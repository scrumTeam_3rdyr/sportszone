import React from 'react';
import { StyleSheet, Dimensions, ScrollView, FlatList,View,Image,TouchableOpacity } from 'react-native';
import { Button, Block, Text, Input, theme } from 'galio-framework';
import { AntDesign } from '@expo/vector-icons'; 
import { SearchBar } from 'react-native-elements';

import { Icon, Product } from '../components';
import Theme from '../constants/Theme';
import firebase from '../firebase';
const { width } = Dimensions.get('screen');

export default class Home extends React.Component {
  state = {
    data: [],
    sdata: [],
  };

componentDidMount () {

    firebase.firestore().collection("player").onSnapshot((querySnapshot) => {
      const list = [];
      const user = firebase.auth().currentUser.email;
      querySnapshot.forEach(doc => {
        const { email,username,imageURL} = doc.data();
        if (email !== user){
          if(imageURL == null){
            list.push({
              id: doc.id,
              title: username,
              logo:'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT3BA8Cz7zVmsHKaVMLhpaL1LwnwFClTjXgMg&usqp=CAU',
              
          });
          }else{
            list.push({
              id: doc.id,
              title: username,
              logo:imageURL,
              
          });
          }
          
          }
      });

      this.setState({data: list,sdata: list});
      
  });
  
}

playerPress(id_num){
  console.log("Pressed");
  const {navigation} = this.props;
  const {teamname} = this.props.route.params;
  const teams= firebase.firestore().collection('team');
  const players= firebase.firestore().collection('player');
  const user = firebase.auth().currentUser.email;
  const title = this.state.data.filter(x => x.id === id_num)[0].title;
  const logo = this.state.data.filter(x => x.id === id_num)[0].logo;
  teams.where("teamName", "==", teamname)
  .get()
  .then(function(querySnapshot) {
      console.log("query ran");
      querySnapshot.forEach(function(doc) {
        console.log("forEach")
        console.log(doc.id);
        teams.doc(doc.id).update( {
          currentPlayers: firebase.firestore.FieldValue.arrayUnion( id_num )
       });
       teams.doc(doc.id).update( {
        id: doc.id
     });
     teams.doc(doc.id).update( {
      currentPlayers: firebase.firestore.FieldValue.arrayUnion( user )
    });

      });
  })
  .catch(function(error) {
      console.log("Error getting documents: ", error);
  });
   
  
  // navigation.navigate('PlayerProfile',{title: title, picture: logo, id:id_num});
  players.where("email", "==", id_num)
  .get()
  .then(function(querySnapshot) {
      console.log("query ran");
      querySnapshot.forEach(function(doc) {
        console.log("forEach")
        console.log(doc.id);
        players.doc(doc.id).update( {
          current_teams: firebase.firestore.FieldValue.arrayUnion( teamname)
       });
       
     players.doc(user).update( {
      current_teams: firebase.firestore.FieldValue.arrayUnion( teamname )
    });

      });
  })
  .catch(function(error) {
      console.log("Error getting documents: ", error);
  });


}

updateSearch = (val) => {
    this.setState({ search: val });
    // console.log(val);
    // console.log(this.state.sdata);
    //console.log(this.state.search);
    if(val == ""){
      this.setState({data: this.state.sdata})
    }
    else{
      const newData = this.state.sdata.filter(item => {      
        const itemData = `${item.title ? item.title.toUpperCase() : ""}   
        ${item.venue ? item.venue.toUpperCase() : ""}`;
        
         const textData = val.toUpperCase();
          
         return itemData.indexOf(textData) > -1;    
      });
      
      this.setState({ data: newData });  
    }
};



  

  renderProducts = () => {
    const { navigation } = this.props;
    return (
     
      <FlatList
           data={this.state.data}
           renderItem={({item})=>
           
           <View style={styles.list }>
            
           <Image source = {{uri:item.logo}} style={styles.imageView} />
          {/* <View style={{flex:2, flexDirection: 'column',justifyContent:"center", alignItems:"center"}}>
              
          </View> */}
           
           <View style={{flex:4, flexDirection: 'column', alignItems:"flex-end",justifyContent:"center"}}>
           <Text size={16}>{item.title}</Text>
           
           </View>
           <View style={{flex:4, flexDirection: 'column', alignItems:"flex-end",justifyContent:"center", paddingRight:20}}>
           <TouchableOpacity
                onPress={() => this.playerPress(item.id)}>
           <AntDesign name="adduser" size={24} color="green" />
           </TouchableOpacity>
           </View>

         </View>
         
         }
         keyExtractor={item => item.id}
         />


 )
    
  }

  render() {
    const { navigation } = this.props;
    const { search } = this.state;
    return (
      
      <Block flex style={styles.home}>
       
        <SearchBar
            round
            lightTheme
            placeholder="Search..."
            containerStyle={{backgroundColor: theme.COLORS.TRANSPARENT}}
            //inputStyle={{backgroundColor: 'white'}}
            inputContainerStyle={{backgroundColor: theme.COLORS.WHITE}}
            onChangeText={(val) => this.updateSearch(val)}
            //onClearText={someMethod}
            value={this.state.search}
         />
          {this.renderProducts()}
      </Block>

    );
  }
}

const styles = StyleSheet.create({
  home: {
    width: width,    
  },
  search: {
    height: 48,
    width: width - 32,
    marginHorizontal: 16,
    borderWidth: 1,
    borderRadius: 3,
  },
  header: {
    backgroundColor: theme.COLORS.WHITE,
    shadowColor: theme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 8,
    shadowOpacity: 0.2,
    elevation: 4,
    zIndex: 2,
  },
  tabs: {
    marginBottom: 24,
    marginTop: 10,
    elevation: 4,
  },
  tab: {
    backgroundColor: theme.COLORS.TRANSPARENT,
    width: width * 0.50,
    borderRadius: 0,
    borderWidth: 0,
    height: 24,
    elevation: 0,
  },
  tabTitle: {
    lineHeight: 19,
    fontWeight: '300'
  },
  divider: {
    borderRightWidth: 0.3,
    borderRightColor: theme.COLORS.MUTED,
  },
  products: {
  //  width: width - theme.SIZES.BASE * 2,
  //  paddingVertical: theme.SIZES.BASE * 2,
    flex:1,
    flexDirection:"row",
  },
  TouchableOpacityStyle: {
    //Here is the trick
    position: 'absolute',
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    right: 35,
    bottom: 35,
  },
  list:{
    flex:1, 
    flexDirection: 'row', 
    justifyContent:"space-evenly",
    backgroundColor:"#fff",
    borderBottomWidth:10,
    borderBottomColor:Theme.COLORS.BORDER_COLOR,
    padding:1,
    borderRadius:20
  },
  imageView: {
    width: width/5,
    height: width/5 ,
    margin: 7,
    borderRadius : 7,
},

});