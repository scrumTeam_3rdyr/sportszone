import React from 'react';
import { StyleSheet, Dimensions, ScrollView, FlatList,View,Image,TouchableOpacity, Picker, Alert } from 'react-native';
import { Button, Block, Text, Input, theme } from 'galio-framework';

import { Icon, Product } from '../components/';
import Theme from '../constants/Theme';

import DateTimePicker from '@react-native-community/datetimepicker';
import DatePick from './DatePick';
import moment from 'moment';
//import DropDownPicker from 'react-native-dropdown-picker';
//import {Picker} from '@react-native-community/picker';
//import ModalDropdown from 'react-native-modal-dropdown';
// const DropDown = require('react-native-dropdown');
// const {
//   Select,
//   Option,
//   OptionList,
//   updatePosition
// } = DropDown;

import firebase from '../firebase';

import {timeSlotsFetch} from '../components/timeSlotFetch';
import { color } from 'react-native-reanimated';

const { width } = Dimensions.get('screen');
const thumbMeasure = (width - 48 - 32) / 3;

const DATA =[
  {
    id:21,
    title: ' Court 1',
    time:"4.00PM",
    logo:'https://preview.free3d.com/img/2014/02/2202328842931013571/uff2xoft-900.jpg',
    date:"05th July 2020",
    venue:"Colombo 07"
  },
  {
    id:22,
    title: 'Court 2',
    time:"6.30PM",
    logo:'https://i.pinimg.com/474x/25/cb/7b/25cb7b511ad21961ec2c1b625f4527cb--soccer-photography-bangkok.jpg',
    date:"06th July 2020",
    venue:"Negombo"
  },
  {
    id:23,
    title: 'Court 3',
    time:"4.00PM",
    logo:'http://biblus.accasoftware.com/en/wp-content/uploads/sites/2/2019/04/Render-aereo-campo-futsal-software-BIM-architettura-Edificius.jpg',
    date:"07th July 2020",
    venue:"1, galleface, colombo"
  },
  {
    id:124,
    title: 'Colombo Futsal club',
    time:"4.00PM",
    logo:'http://roar.media/english/reports/wp-content/uploads/2014/10/10524686_759213854130883_1461396911850055322_n-550x413.jpg',
    date:"12th July 2020",
    venue:"70 Galle Rd, Dehiwala"
  },
  {
      id:24,
      title: 'CR7',
      time:"4.00PM",
      logo:'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR-RgTEJObX4mxcMOgxz0qrh6CJd1y_jh-4-Q&usqp=CAU',
      date:"12th July 2020",
      venue:"23 Mile Post Ave, Colombo"
    },
];


export default class Home extends React.Component {
  
  // constructor(props){
  //   super(props);
  //   this.state = {
  //     currDate: new Date(),
  //     //selectedDate: currDate.getTime(),
  //     selectedDate: new Date(),
  //   };
  // }

  state = {
    currDate: new Date(),
    //selectedDate: currDate.getTime(),
    selectedDate: new Date().getTime(),
    availableSlots: [],
    holidayStatus: "",
    selectedSlot: "",
    isComplete: false,
    courtImg:[],
  };

  // componentDidMount(){
  //   this.setState({selectedDate: new Date().getTime()})
  // }

  componentDidMount(){
    //const email = this.props.route.params.id;
    // const user = await firebase.firestore()
    //   .collection('court')
    //   .doc(email)
    //   .get();

    this.timeSlotsUpdate();
    const courtId = this.props.route.params.id

    firebase.firestore().collection('court').doc(courtId).get().then(
      (doc)=>{
        console.log("court imgs trig "+ courtId)
          let data = doc.data();
          // setAddress(data.address);
          // setName(data.court_name);
          // if(data.contact_no){setContactNo(data.contact_no)}
          // if(data.imageuri){setProfilePicture(data.imageuri);}
          // if(data.court_images){setCourtImages(data.court_images)}
          // if(data.timeSlot){setTimeSlots(data.timeSlot)}
          this.setState({courtImg: data.court_images});
          console.log("court images "+data);
        }

    )
    .catch();
  

    //console.log(email);
  }

  callbackSlots = (availSlots) => {
    console.log("slotfetchcallback " + availSlots)
    if(typeof availSlots === 'undefined'){
      console.log("undefined slot array trig")
    }else if(availSlots == 0){
      this.setState({holidayStatus: true});
    }else{
      this.setState({holidayStatus: false});
      this.setState({availableSlots: availSlots})
    }
  }

  timeSlotsUpdate(){
    const courtEmail = this.props.route.params.id;
    const date = moment(this.state.selectedDate).format("DDMMMYYYY").toString();
    
    timeSlotsFetch(courtEmail, date, this.callbackSlots);
    console.log("available slots " + this.state.availableSlots);
    // if(this.state.availableSlots==0){
    //   this.setState({holidayStatus: true});
    // }
    // else{
    //   this.setState({holidayStatus: false});
    // }
  }

  // componentDidUpdate(prevProps, prevState){
  //   //const email = this.props.route.params.id;
  //   const prevd = moment(prevState.selectedDate).format("DDMMMYYYY").toString();
  //   const tod = moment(this.state.selectedDate).format("DDMMMYYYY").toString();

  //   if (prevd !== tod){
  //     this.timeSlotsUpdate();
  //   }
  // }

  leaderBoardPress = () => {
      const email = this.props.route.params.id;
      //console.log('Leaderboard pressed '+email);
  }

  matchHistoryPress = () => {
    const email = this.props.route.params.id;
    //console.log('Leaderboard pressed '+email);
  }

  courtBook = () => {
    const courtEmail = this.props.route.params.id;
    const courtName = this.props.route.params.title;
    const date = moment(this.state.selectedDate).format("DDMMMYYYY").toString();
    const bookSlot = this.state.selectedSlot;
    const playerId = firebase.auth().currentUser.email;
    const dateSlot = moment(this.state.selectedDate).format("YYYY-MM-DD").toString()+"_"+bookSlot;
    const courtLogo = this.props.route.params.picture;

    if(this.state.isComplete){
      firebase.firestore()
        .collection('court')
        .doc(courtEmail)
        .collection('availabletimeslots')
        .doc(date)
        .collection('timeslots')
        .doc(bookSlot)
        .set({
          'booked':true,
          'player': playerId,
          'timeslot': bookSlot
          },
          {merge: true}
        )
        .then(() => {
          console.log('Court booked ' + playerId );
          console.log(playerId + " " + date + " " + bookSlot + " " + courtEmail)
        })
        .catch(error => {
          console.log(error.message)
          console.log(playerId + " " + date + " " + bookSlot + " " + courtEmail)
        });

        firebase.firestore()
        .collection('player')
        .doc(playerId)
        .collection('court_bookings')
        .doc(dateSlot)
        .set({
          'active':true,
          'courtID': courtEmail,
          'date': this.state.selectedDate,
          'time_slot': bookSlot,
          'court_logo': courtLogo,
          'court_name':courtName
          },
          {merge: true}
        )
        .then(() => {
          console.log('Court booking in player ' + playerId +" " + dateSlot );
          //console.log(playerId + " " + date + " " + bookSlot + " " + courtEmail)
        })
        .catch(error => {
          console.log(error.message)
          //console.log(playerId + " " + date + " " + bookSlot + " " + courtEmail)
        });
    }
    console.log("Courtbook trig " + courtEmail)
    //console.log(playerId + " " + date + " " + bookSlot + " " + courtEmail)
  }

  courtPress(){
    console.log("pressed")
  }

  showAlert1 = () => {  
    const courtName = this.props.route.params.title;
    const date = moment(this.state.selectedDate).format("dddd MMM Do YYYY").toString();
    const bookSlot = this.state.selectedSlot;
    Alert.alert(  
        'Confirm Booking',  
        'Book ' + courtName + " on " + date + " at " + bookSlot,  
        [  
          //{ onDismiss: () => {} },
          {  
            text: 'Cancel',  
            onPress: () => console.log('Cancel Pressed'),  
            style: 'cancel',  
          },  
            {text: 'Confirm', onPress: this.courtBook},  
        ]  
    );  
  }  

  showAlert2 = () => {  
    
    Alert.alert(  
      'Cannot book',  
      'Court not avilable on this date',  
      [  
        {  
          text: 'Close',  
          onPress: () => console.log('Cancel Pressed'),  
          style: 'cancel',  
        },  
          //{text: 'OK', onPress: this.courtBook},  
      ]  
  );  
  }  

  renderProducts = () => {
    return (

      <FlatList
           data={DATA}
           renderItem={({item})=>

               <View style={styles.list}>

                   //<Image source = {{uri:item.logo}} style={styles.imageView} />

                    <View style={{flex:2, flexDirection: 'column',justifyContent:"flex-start", alignItems:"flex-start"}}>
                        <TouchableOpacity style={{flex:2, flexDirection: 'column',justifyContent:"center", alignItems:"center"}} onPress={this.courtPress}>
                            <View style={{flex:2, flexDirection: 'column',justifyContent:"center", alignItems:"center"}}>
                                <Text size={16} style={{fontWeight: "bold"}}>
                                    {item.title}
                                </Text>
                            </View>
                        </TouchableOpacity>

                       <View style={{flex:4, flexDirection: 'column', alignItems:"flex-end",justifyContent:"center", paddingRight:20}}>
                           <Text></Text>
                           <Text ></Text>
                           <Text>{item.venue}</Text>
                       </View>
                   </View>



               </View>
             }
         keyExtractor={item => item.id}
         />


 )

  }

  callbackDate = (sdate) => {
    console.log("parent " + sdate)
    this.setState({selectedDate: sdate})
    this.timeSlotsUpdate()
  }

  render() {
  const {navigation} = this.props;
  const prof_name = this.props.route.params.title;
  const { currDate, selectedDate } = this.state;
  const address = this.props.route.params.venue;
  console.log("address "+address);

  const CourtImages = this.state.courtImg;
  //this.setState({selectedDate : this.currDate.getTime()});
  //console.log(currDate +" cort profile render "+this.state.selectedDate);

  //const availSlotsComponents = (typeof availSlots === 'undefined')? <Text>Not available</Text>:this.state.availableSlots.map(slot => <Picker.Item label={slot} value={slot} />);

  let slotView;
  if(this.state.holidayStatus){
    this.state.isComplete = false;
    slotView = <Text style={{color: 'lightslategrey', fontSize: 18, fontWeight: 'bold'}}>Court closed on this date</Text> 
  }else if(this.state.availableSlots.length == 0){
    this.state.isComplete = false;
    slotView = <Text style={{color: 'darkcyan'}}>All booked up </Text> 
  } else if(typeof this.state.availableSlots !== 'undefined'){
    this.state.isComplete = true;
    slotView =  <Picker
    selectedValue={this.state.selectedSlot}
    style={{height: 50, width: 160}}
    onValueChange={(itemValue, itemIndex) =>
      this.setState({selectedSlot: itemValue})
    }>
    {/* {availSlotsComponents} */}
    {this.state.availableSlots.map(slot => <Picker.Item label={slot} value={slot} />)}
  </Picker> 
  }

    return (
      <Block flex style={styles.home}>
        <Image source = {{uri:this.props.route.params.picture}} style={styles.imageViewHead}/>
        <View style={{flexDirection:"row", justifyContent:'space-around'}}>
          <TouchableOpacity onPress = {this.matchHistoryPress}>
              <Text style = {styles.text1}>
                Match history
              </Text>
          </TouchableOpacity>

          <TouchableOpacity onPress = {this.leaderBoardPress}>
              <Text style = {styles.text1}>
                LeaderBoard
              </Text>
          </TouchableOpacity>
        </View>
        
        <View style={{flexDirection: 'row', marginTop:10, marginBottom:5}}>
          <View style = {{flex: 0.75, backgroundColor: 'darkseagreen'}}>
            <DatePick selectDate = {this.callbackDate}/>

            <View style={{alignItems:'center'}}>
            {slotView}
            </View>
          </View>
          <View style = {{flex: 0.25, backgroundColor:'lightgreen', marginLeft: 7, justifyContent:'center'}}>
            <TouchableOpacity style = {{alignItems: 'center'}} onPress = {this.state.isComplete? this.showAlert1 : this.showAlert2}>
                <Text style = {this.state.isComplete? styles.text2: styles.text3}>
                  Book Now
                </Text>
            </TouchableOpacity>
          </View>
        </View>

        {/* <Text>{moment(this.state.selectedDate).format("dddd MMM Do YYYY")}</Text>
        <Text>{this.state.selectedSlot}</Text>
        <Text>  Profile  </Text> */}
        <View style={{flex: 0.1}}>

        </View>
        <View style={{}}>
        <Text>{this.props.route.params.phone}</Text>
        {/* <Text> </Text> */}
          <Text>{this.props.route.params.address}</Text>
        </View>
        <View style={{marginLeft:10, marginRight:10}}>
        <Block row space="between" style={{ flexWrap: 'wrap' }} >
                {CourtImages? CourtImages.map((img, imgIndex) => (
                  <Image
                    source={{ uri: img }}
                    key={`viewed-${img}`}  
                    resizeMode="cover"
                    style={styles.thumb}
                  />
                )): <Image
                source={{ uri: "https://st.depositphotos.com/1007097/3878/i/950/depositphotos_38780731-stock-photo-futsal-court.jpg" }}
                key={`viewed-https://st.depositphotos.com/1007097/3878/i/950/depositphotos_38780731-stock-photo-futsal-court.jpg`}  
                resizeMode="cover"
                style={styles.thumb}
              />}
              </Block>
        </View>

      </Block>

    );
  }
}

const styles = StyleSheet.create({
  home: {
    width: width,
  },
  search: {
    height: 48,
    width: width - 32,
    marginHorizontal: 16,
    borderWidth: 1,
    borderRadius: 3,
  },
  header: {
    backgroundColor: theme.COLORS.WHITE,
    shadowColor: theme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 8,
    shadowOpacity: 0.2,
    elevation: 4,
    zIndex: 2,
  },
  tabs: {
    marginBottom: 24,
    marginTop: 10,
    elevation: 4,
  },
  tab: {
    backgroundColor: theme.COLORS.TRANSPARENT,
    width: width * 0.50,
    borderRadius: 0,
    borderWidth: 0,
    height: 24,
    elevation: 0,
  },
  tabTitle: {
    lineHeight: 19,
    fontWeight: '300'
  },
  divider: {
    borderRightWidth: 0.3,
    borderRightColor: theme.COLORS.MUTED,
  },
  products: {
  //  width: width - theme.SIZES.BASE * 2,
  //  paddingVertical: theme.SIZES.BASE * 2,
    flex:1,
    flexDirection:"row",
  },
  list:{
    flex:1,
    flexDirection: 'row',
    justifyContent:"space-evenly",
    backgroundColor:"#fff",
    //borderRadius:25,
    borderBottomWidth:10,
    borderBottomColor:Theme.COLORS.BORDER_COLOR,
    paddingVertical:10,
  },
  imageView: {
    width: width/5,
    height: width/5 ,
    margin: 7,
    borderRadius : 7,
    },
   TouchableOpacityStyle: {
     //Here is the trick
     position: 'absolute',
     width: 50,
     height: 50,
     alignItems: 'center',
     justifyContent: 'center',
     right: 35,
     bottom: 35,
   },
   imageViewHead:{
     width: width,
     height: width/2,
       alignSelf:"center"
   },
   text1: {
     marginTop: 3,
    padding: 15,
    borderColor: 'black',
    backgroundColor: 'lemonchiffon',
    fontSize: 18
    },
   text2: {
      marginLeft: 7,
     
     borderColor: 'black',
     
     fontSize: 18
   },
   text3: {
    marginLeft: 7,
   borderColor: 'black',
   fontSize: 18,
   color: 'darkseagreen'
  },
  thumb: {
    borderRadius: 4,
    marginVertical: 4,
    alignSelf: 'center',
    width: thumbMeasure,
    height: thumbMeasure
  }


});
