import React, { useState } from 'react';
import { StyleSheet, Dimensions, ScrollView,View ,TextInput,Image} from 'react-native';
import { Button, Block, Text, Input, theme } from 'galio-framework';

import { Icon, Product } from '../components/';
import { Header,ListItem, PricingCard ,Divider } from 'react-native-elements';
import { Images, materialTheme } from '../constants';
const { width,height } = Dimensions.get('screen');

//import products from '../constants/products';
import firebase from '../firebase';
import 'firebase/firestore';



import {
  AdMobBanner,
  AdMobInterstitial,
  PublisherBanner,
  AdMobRewarded,
  setTestDeviceIDAsync,
} from 'expo-ads-admob';


//import { GradientHeader } from "react-native-gradient-header";
const { AsyncStorage } = require('react-native');
const user=firebase.auth().currentUser;
const colref=firebase.firestore().collection('player'); 






export default class Home extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
        player:[]
    };
  }

  componentDidMount() {  
    //console.log("component mount : ",user.email);
    var usermail=firebase.auth().currentUser.email.toString();
    //const docref=colref.doc(usermail);
    console.log("component mount : ",usermail);
    colref.doc(usermail).get().then(function(doc) {
      if (doc.exists) {
          console.log("Document data:", doc.data());
          //get all open challenges and get the count which is not due
          colref.doc(usermail).collection('openChallenge').where(data)
      } else {
          // doc.data() will be undefined in this case
          console.log("No such document!");
      }
    }).catch(function(error) {
      console.log("Error getting document:", error);
    });
  }


  render() {
    const {navigation} = this.props;
    return (

      
      <Block flex style={styles.home}>
      <View >
          
          <Image source = {require('../assets/images/home_clipart.png')} style={styles.imageViewHead} />
          <Text style={styles.headText}>SPORTS ZONE</Text>
        </View>


       

<View style={styles.banner}>
<AdMobBanner
  bannerSize="banner"
  adUnitID="ca-app-pub-3940256099942544/6300978111" // Test ID, Replace with your-admob-unit-id
  servePersonalizedAds  = "false"// true or false
  onDidFailToReceiveAdWithError={this.bannerError} />
  </View>

  <View style={styles.trophy_wrapper}>
          
          <Image source = {{uri:"https://i.ya-webdesign.com/images/trophy-vector-png-4.png"}} style={styles.trophyimageView} />
          <Text style={styles.trophy_text}>OPEN CHALLENGES</Text>
        </View>

        <View style={styles.banner}>
<AdMobBanner
  bannerSize="largeBanner"
  adUnitID="ca-app-pub-3940256099942544/6300978111" // Test ID, Replace with your-admob-unit-id
  servePersonalizedAds  = "false"// true or false
  onDidFailToReceiveAdWithError={this.bannerError} />
  </View>
        

</Block>

    );
  }
}

const styles = StyleSheet.create({
  home: {
    width: width,
    height:height    
  },
  search: {
    height: 48,
    width: width - 32,
    marginHorizontal: 16,
    borderWidth: 1,
    borderRadius: 3,
  },
  header: {
    backgroundColor: theme.COLORS.WHITE,
    shadowColor: theme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 8,
    shadowOpacity: 0.2,
    elevation: 4,
    zIndex: 2,
  },
  banner: { justifyContent: "center", alignItems: "center" },
  tabs: {
    marginBottom: 24,
    marginTop: 10,
    elevation: 4,
  },
  tab: {
    backgroundColor: theme.COLORS.TRANSPARENT,
    width: width * 0.50,
    borderRadius: 0,
    borderWidth: 0,
    height: 24,
    elevation: 0,
  },
  tabTitle: {
    lineHeight: 19,
    fontWeight: '300'
  },
  divider: {
    borderRightWidth: 0.3,
    borderRightColor: theme.COLORS.MUTED,
  },
  products: {
    width: width - theme.SIZES.BASE * 2,
    paddingVertical: theme.SIZES.BASE * 2,
  },
  imageView: {
    width: width/5,
    height: width/5 ,
    margin: 7,
    borderRadius : 7,
},
head:{
  marginTop: width/10,
  width:width,
  height:width/3,
  backgroundColor: "#3BAD36",
  justifyContent:"center",
},
headText:{
  padding:10,

  alignSelf:"center",
  fontSize:30,
  color:"black",
  
},
imageViewHead:{
  opacity: 0.5,
  width: width/1,
    height: height/4.5 ,
    marginTop: 20,
    borderRadius : 3,
    alignSelf:"center"
},

trophyimageView:{
  width: width/6,
    height: height/7 ,
    margin: 7,
    borderRadius : 7,
    alignSelf:"center"
},
trophy_wrapper:{
  
  marginTop:10,
  alignSelf:"center",
  borderRadius:120,
  marginBottom:5,
  width:width-20,
  height:height/5,
  backgroundColor:"#12a15c",//"#3BAD36"
  justifyContent:"center",
},
trophy_text:{
  alignSelf:"center",
  fontSize:20,
  color:"white",
  paddingBottom:height/45
  
}
});
