import React, {useState,useEffect} from 'react';
import { StyleSheet, Dimensions, ScrollView, FlatList, TouchableOpacity } from 'react-native';
import { Button, Block, Text, Input, theme } from 'galio-framework';
import { Icon, Product } from '../components/';
import MatchResultItem from '../components/MatchResultItem';
import { matches } from '../Data/DummyData';
import firebase from '../firebase';
import Match from '../Models/match';

const { width, height } = Dimensions.get('screen');

const getMatches = async (court) => {
    const db = firebase.firestore()
    
    const ref = db.collection("court").doc(court).collection('matches');
    const matchesSnapshot = await ref.get();
    const matches = matchesSnapshot.docs.map(matchSnapshot => {
        return matchSnapshot.data();
    })
    const loadedmatches = [];
    for (const key in matches) {// noway to filter dates
        if(matches[key].isResultsUpdated){
            loadedmatches.push(new Match(
                matches[key].id,
                matches[key].team1Id,
                matches[key].team1Name,
                matches[key].team1ImageUrl,
                matches[key].team2Id,
                matches[key].team2Name,
                matches[key].team2ImageUrl,
                matches[key].date,
                matches[key].time_slot,
                matches[key].isResultsUpdated,
                matches[key].winner, //id
            ));
            }
    }
    return loadedmatches;
}


const ResultsScreen = props => {

    //const courtEmail = props.route.params.email;
    const court = props.route.params.email;
    const [matchData,setMatchData] = useState([]);

    useEffect(() => {
        const getMatchData = async () => {
            const data = await getMatches(court);
            setMatchData(data);
        };
        getMatchData();
    }, [setMatchData]);

    const renderTeams = itemData => {

        return (
            <MatchResultItem
                team1Name={itemData.item.team1Name}
                team1Id={itemData.item.team1Id}
                logo1={itemData.item.team1ImageUrl}
                team2Name={itemData.item.team2Name}
                team2Id={itemData.item.team2Id}
                logo2={itemData.item.team2ImageUrl}
                date={itemData.item.date}
                time={itemData.item.time}
                winner={itemData.item.winner}
            />
        );
    };
    return (
        <Block style={styles.screen}>

            <FlatList
                data={matchData}
                renderItem={renderTeams}
            />
            
        </Block>




    );
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: '#dedfda'

    },
    TouchableOpacityStyle: {
        //Here is the trick
        position: 'absolute',
        width: width * 0.15,
        height: width * 0.15,
        alignItems: 'center',
        justifyContent: 'center',
        right: 45,
        bottom: 45,
        backgroundColor: "#3BAD36",
        borderRadius: 50
    },
});

export default ResultsScreen;