import React from 'react';
import { Easing, Animated, Dimensions,TouchableOpacity } from 'react-native';
import { createStackNavigator } from "@react-navigation/stack";
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'

import { Block, Text, theme } from "galio-framework";

import HomeScreen from '../screens/Home';
import OnboardingScreen from '../screens/Onboarding';
import ProfileScreen from '../screens/Profile';
import ShareScreen from '../screens/ShareSS';
import TeamScreen from '../screens/Teams';
import CreateTeamScreen from '../screens/CreateTeam';
import AddplayerScreen from '../screens/Addplayer';
import ViewTeamScreen from '../screens/ViewTeam';
import NewMatchStartScreen from '../screens/NewMatchStartScreen';
import NewMatchSelectTeam from '../screens/NewMatchSelectTeam';
import SettingsScreen from '../screens/Settings';
import MyMatchesScreen from '../screens/MyMatches';
import MatchRequestsScreen from '../screens/MatchRequests';
import OpenChallengesScreen from '../screens/OpenChallenge';
import ArchiveMatchesScreen from '../screens/ArchiveMatches';
import CourtsScreen from '../screens/CourtList';
import LeaderboardScreen from '../screens/LeaderboardScreen';
import ResultsScreen from '../screens/ResultsScreen';
import CourtBookings from '../screens/CourtBookings';
// New login and signup routes
import LoginScreen from '../screens/LoginPlayer';
import SignupScreen from '../screens/SignupPlayer';
import CourtProfile from '../screens/CourtProfile';

import {Icon} from 'galio-framework';

import { Header } from '../components';
import { Images, materialTheme } from "../constants/";

//import firebase auth -isuru
import firebase from '../firebase';
import 'firebase/firestore'
import "firebase/auth";

const { width } = Dimensions.get("screen");

const Stack = createStackNavigator();
const TopTab = createMaterialTopTabNavigator();
const Tab=createBottomTabNavigator()

const profile = {
  avatar: Images.Profile,
  name: "Rachel Brown",
  type: "Seller",
 // plan: "Free",
  rating: 4.8
};

function signOut(props) {
  const {navigation} = props;
  firebase.auth()
  .signOut()
  .then(() => {
    console.log('User signed out!');
    navigation.navigate("Login");
  });
}

function ProfileStack(props) {
  const {navigation} = props;
  return (
    <Stack.Navigator initialRouteName="Profile" mode="card" headerMode="screen">
      <Stack.Screen
        name="Profile"
        component={ProfileScreen}
        options={{
          headerTitle:"Profile",
          headerTitleAlign:"center",
          headerRight:()=>(
          <TouchableOpacity onPress={()=>signOut(props)}>
          <Icon name="power" family="material-community" size={30} style={{paddingRight:10,}}/>
          </TouchableOpacity>),
          headerStyle:{backgroundColor:materialTheme.COLORS.GREEN}
        }}
      />
    </Stack.Navigator>
  );
}

function TeamsStack(props) {

  return (
    <Stack.Navigator initialRouteName="Team" mode="card" headerMode="screen">
      <Stack.Screen
        name="Team"
        component={TeamScreen}
        options={{
          headerTitle:"Teams",
          headerTitleAlign:"center",
          headerStyle:{backgroundColor:materialTheme.COLORS.GREEN}
        }}
      />
      <Stack.Screen
        name="View Team"
        component={ViewTeamScreen}
        options={{
          headerTitle:"Players",
          headerTitleAlign:"center",
          headerStyle:{backgroundColor:materialTheme.COLORS.GREEN}
        }}
      />
      <Stack.Screen
        name="Create Team"
        component={CreateTeamScreen}
        options={{
          headerTitle:"Create New Teams",
          headerTitleAlign:"center",
          headerStyle:{backgroundColor:materialTheme.COLORS.GREEN}
          
        }}
      />
      <Stack.Screen
        name="Add Player"
        component={AddplayerScreen}
        options={{
          headerTitle:"Add new payer",
          headerTitleAlign:"center",
          headerStyle:{backgroundColor:materialTheme.COLORS.GREEN}
          
        }}
      />
    </Stack.Navigator>
  );
}

// Login



function SettingsStack(props) {
  return (
    <Stack.Navigator
      initialRouteName="Settings"
      mode="card"
      headerMode="screen"
    >
      <Stack.Screen
        name="Settings"
        component={SettingsScreen}
        options={{
          header: ({ navigation }) => (
            <Header title="Settings"  navigation={navigation} />
          )
        }}
      />
        
    </Stack.Navigator>
  );
}





function NewMtachStack(props) {
  return (
    <Stack.Navigator
      initialRouteName="New Match"
      mode="card"
      headerMode="screen"
    >
      <Stack.Screen
        name="NewMatchStartScreen"
        component={NewMatchStartScreen}
        options={{
          header: ({ navigation }) => (
            <Header 
            title="New Match"
            navigation={navigation}
             transparent
            
             />
          ),
          
          
        }}
      />
        {/* <Stack.Screen
        name="Settings"
        component={NewMatchSelectTeam}
        options={{
          header: ({ navigation }) => (
            <Header title="NewMatch"  navigation={navigation} />
          )
        }}
      >

      </Stack.Screen> */}
     <Stack.Screen
        name="NewMatchSelectTeam"
        component={NewMatchSelectTeam}
        options={{
          header: ({ navigation }) => (
            <Header back title="New Match"  navigation={navigation} transparent />
          )
        }}
      />
    </Stack.Navigator>
  );
}
// function MatchesResultsStack(props) {
//   return (
//     <Stack.Navigator mode="card" headerMode="screen" 
//     options={{
//       headerTitleAlign:"center"
//     }}>
//        <Stack.Screen 
//         name="matchResults"
//         component={MatchResultsTab}
//         options={{
//           headerTitle:"Match Results",
//           headerTitleAlign:"center",
//           headerStyle:{backgroundColor:materialTheme.COLORS.GREEN}
//         }}
        
//       />
      
//     </Stack.Navigator>
//   );
// }


// function MatchResultsTab(props) {
//   return (
//     <TopTab.Navigator mode="card" headerMode="screen" tabBarOptions={{pressColor:"#3BAD36",indicatorStyle:{backgroundColor:"#3BAD36"}}}>
//       <TopTab.Screen name="results" component={ResultsScreen}/>
//       <TopTab.Screen name="leaderboard" component={LeaderboardScreen}  />
//     </TopTab.Navigator>
//   );
// }
function ComponentsStack(props) {
  return (
    <Stack.Navigator mode="card" headerMode="screen">
      <Stack.Screen
        name="Components"
        component={ComponentsScreen}
        options={{
          header: ({ navigation, scene }) => (
            <Header title="Components" scene={scene} navigation={navigation} />
          )
        }}
      />
    </Stack.Navigator>
  );
}
function CourtsStack(props) {
  return (
    <Stack.Navigator mode="card" headerMode="screen">
      <Stack.Screen
        name="Courts"
        component={CourtsTab}
        options={{
          headerTitle:"Courts",
          headerTitleAlign:"center",
          headerStyle:{backgroundColor:materialTheme.COLORS.GREEN}
        }}
      />
      <Stack.Screen
          name="CourtProfile"
          component={CourtProfile}
           options={
              ({ route }) => ({ title: route.params.title, headerTitleAlign:"center" })
            }
       />
      
       <Stack.Screen
        name="matchResult"
        component={ResultsScreen}
        options={{
          headerTitle:"Match Results",
          headerTitleAlign:"center",
          headerStyle:{backgroundColor:materialTheme.COLORS.GREEN}
        }}
      />
       <Stack.Screen
        name="Leaderboard"
        component={LeaderboardScreen}
        options={{
          headerTitle:"Leaderboard",
          headerTitleAlign:"center",
          headerStyle:{backgroundColor:materialTheme.COLORS.GREEN}
        }}
      />
         
    </Stack.Navigator>
  );
}

function CourtsTab(props) {
  return (
    <TopTab.Navigator mode="card" headerMode="screen" tabBarOptions={{pressColor:"#3BAD36",indicatorStyle:{backgroundColor:"#3BAD36"}}}>
      <TopTab.Screen name="All Courts" component={CourtsScreen}/>
      <TopTab.Screen name="My Bookings" component={CourtBookings}  />
    </TopTab.Navigator>
  );
}


function MatchesStack(props) {
  return (
    <Stack.Navigator mode="card" headerMode="screen" 
    options={{
      headerTitleAlign:"center"
    }}>
      <Stack.Screen
        name="My Matches"
        component={MatchesTab}
        options={({navigation})=>({
          headerTitle:"My Matches",
          headerTitleAlign:"center",
          headerRight:()=>(
            <TouchableOpacity onPress={()=>navigation.navigate("Archive")}>
            <Icon name="archive" family="material-community" size={30} style={{paddingRight:10,}}/>
            </TouchableOpacity>),
          headerStyle:{backgroundColor:materialTheme.COLORS.GREEN}
        })}
      />
      <Stack.Screen
        name="Archive"
        component={ArchiveMatchesScreen}
        options={{
          headerTitle:"Matches Archive",
          headerTitleAlign:"center",
          headerStyle:{backgroundColor:materialTheme.COLORS.GREEN}
        }}
      />
      <Stack.Screen
        name="New Match"
        component={NewMatchStartScreen}
        options={{
          headerTitle:"New Match",
          headerTitleAlign:"center",
          headerStyle:{backgroundColor:materialTheme.COLORS.GREEN}
        }}
      />
      <Stack.Screen
        name="NewMatchSelectTeam"
        component={NewMatchSelectTeam}
        options={{
          headerTitle:"Select team",
          headerTitleAlign:"center",
          headerStyle:{backgroundColor:materialTheme.COLORS.GREEN}
        }}
      />
    </Stack.Navigator>
  );
}
function MatchesTab(props) {
  return (
    <TopTab.Navigator mode="card" headerMode="screen" tabBarOptions={{pressColor:"#3BAD36",indicatorStyle:{backgroundColor:"#3BAD36"}}}>
      <TopTab.Screen name="Upcoming Matches" component={MyMatchesScreen}/>
      <TopTab.Screen name="Match Requests" component={MatchRequestsScreen}  />
      <TopTab.Screen name="Open Challenges" component={OpenChallengesScreen}  />
    </TopTab.Navigator>
  );
}

function OpenchallengesTab(props) {
  return (
    <TopTab.Navigator mode="card" headerMode="screen" tabBarOptions={{pressColor:"#3BAD36",indicatorStyle:{backgroundColor:"#3BAD36"}}}>
      <TopTab.Screen name="Open Challenges" component={OpenChallengesScreen}  />
      <TopTab.Screen name="Upcoming Matches" component={MyMatchesScreen}/>
      <TopTab.Screen name="Match Requests" component={MatchRequestsScreen}  />
    </TopTab.Navigator>
  );
}
function HomeStack(props) {
  return (
    <Stack.Navigator mode="card" headerMode="screen">
      <Stack.Screen 
        name="Home"
        component={HomeScreen}
        options={{
          headerTitle:"Home",
          headerTitleAlign:"center",
          headerStyle:{backgroundColor:materialTheme.COLORS.GREEN},
          headerLeft:null
        }}
      />

      <Stack.Screen 
        name="MyMatches"
        component={MatchesTab}
        
      />
      <Stack.Screen 
        name="OpenChallenges"
        component={OpenchallengesTab}
      />
    </Stack.Navigator>
  );
}



function AppStack(props) {
  return (
    <Tab.Navigator
    initialRouteName="Home"
      tabBarOptions={{
        activeTintColor: '#3BAD36',
       // inactiveTintColor:'#000'
      }}
      >
      <Tab.Screen name="Home" component={HomeStack}
        options={{tabBarLabel: 'Home',
        tabBarIcon: ({focused}) => <Icon name='ios-home'
                                family='ionicon'
                                size={26} 
                                color={focused ? "#3BAD36" : "#000"}/>,
        
        
      }}
        />
      <Tab.Screen name="Matches" component ={MatchesStack}
      options={{tabBarLabel: 'Matches',
      tabBarIcon: ({focused}) => <Icon name='soccer'
                              family='material-community'
                              size={26} 
                              color={focused ? "#3BAD36" : "#000"}/>,
      
    }}
      />
      <Tab.Screen name="Find Court" component ={CourtsStack}
      options={{tabBarLabel: 'Courts',
      tabBarIcon: ({focused}) => <Icon name='soccer-field'
                              family='material-community'
                              size={26} 
                              color={focused ? "#3BAD36" : "#000"}/>,
    }}
      />
      <Tab.Screen name="Teams" component ={TeamsStack}
      options={{tabBarLabel: 'Teams',
      tabBarIcon: ({focused}) => <Icon name='users'
                              family='font-awesome'
                              size={26} 
                              color={focused ? "#3BAD36" : "#000"}/>,
    }}
      />
     
      
      <Tab.Screen name="Profile" component={ProfileStack}
       options={{tabBarLabel: 'Profile',
       tabBarIcon: ({focused}) => <Icon name='user'
                               family='font-awesome'
                               size={26} 
                               color={focused ? "#3BAD36" : "#000"}/>,

     }}
      />
    </Tab.Navigator>
  );
}

export default function OnboardingStack(props) {
  return (
    <Stack.Navigator mode="card" headerMode="none" initialRouteName={firebase.auth().currentUser ? 'App' : 'Login'}>
      <Stack.Screen
        name="Onboarding"
        component={OnboardingScreen}
        option={{
          headerTransparent: true
        }}
      />
      <Stack.Screen
        name="Home"
        component={HomeScreen}
        option={{
          headerTransparent: true,
          headerLeft:null
        }}
      />
      {/* <Stack.Screen
        name="CreateTeam"
        component={CreateTeamScreen}
        option={{
          headerTransparent: true
        }}
      /> */}
      <Stack.Screen
        name="Profile"
        component={ProfileScreen}
        option={{
          headerTransparent: true
        }}
      />
      <Stack.Screen
        name="ShareSS"
        component={ShareScreen}
        options={{
          headerTitle:"My Progress",
          headerTitleAlign:"center",
          headerStyle:{backgroundColor:materialTheme.COLORS.GREEN}
        }}
      />
      <Stack.Screen
        name="Login"
        component={LoginScreen}
        option={{
         // headerTransparent: true
        }}
      />
      <Stack.Screen
        name="Signup"
        component={SignupScreen}
        option={{
          headerTransparent: true
        }}
      />
      <Stack.Screen name="App" component={AppStack} />
    </Stack.Navigator>
  );
}
